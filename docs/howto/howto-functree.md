## 1: How to use mmap treefunc (real case)

In this how-to, we perform the Marauder's map *treefunc* on a real case example, Nek5000, which is a fast and scalable open source CFD solver. Then we generate the associated *treefunc* figure and show useful information out of it. 

This how-to should help you perform Marauder's map *tree func* on your own case and figure out what useful information you can get from it, and how.


### Step One: performing mmap treefunc

See the tutorial for details on how to use the Marauder’s map *treefunc*. 
The mmap_in.yml input file should be set up as follows: The mmap_in.yml input file should be set up as follows (with the right path and package name): 

```Bash
# the path where your sources are stored
path : /Users/desplats/TEST/flinter/Gitlab/Nek5000/core.py
# name of the package
package: Nek5000

# blacklist of unwanted nodes in the graph
remove_patterns :
  - "matplotlib*"
  - "numpy*"
  - "tkinter*"
  - "json*"
  - "PIL*"

soften_patterns :
  - "*BaseForm*"

```

Once this is done, we write the following command on the terminal: 

```Bash
mmap treefunc
```

The terminal's answer is the following:

```Bash
Generating Nek5000 / func_tree.json
```

The *treefunc*'s data is in the json file. Now let's show it as a tree architecture. 

### Step Two: Showing the treefunc graph

In order to visualise the *treefunc*, we write the following command:

```bash
mmap showtree complexity -f mmap_in.yml
```

We obtain the following figure, where we can easily navigate in the software's files and functions architecture. 

![functree-ccn](images/Nek5000_functree_ccn.png)

This *functree* shows the software's folders and files architecture. Unlike the macro *file tree* which stops at files, *func tree* shows the different functions inside the file as well. *mmap tree* thus allows to have deeper vision. Here, as asked with the option on the command that was performed, the coloration is donc by cyclomatic complexity. 

To show the *treefunc* colored by size instead, we write the following command:

```bash
mmap showtree size -f mmap_in.yml
```

And we obtain this figure:

![functree_size](images/Nek5000_functree_size.png)
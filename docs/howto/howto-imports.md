## 2: How to use mmap imports (real case)

In this how-to, we perform the Marauder's map *imports* on a real case example, Nek5000, which is a fast and scalable open source CFD solver. Then we generate the associated *imports graph* figure and show useful information out of it. 

This how-to should help you perform Marauder's map *imports* and *showgraph* on your own case and figure out what useful information you can get from it, and how.


### Step One: performing mmap imports

See the tutorial for details on how to use the Marauder’s map *imports*. 

Write the following command on the terminal: 

```Bash
mmap imports
```

The terminal's answer is the following:

```Bash
INFO - Generating imports database
INFO - Imports graph generated
Generating nek5000/imports.json.
```

The data is in the imports.json file. Now let's show it as an imports graph. 

### Step Two: Showing the imports graph

In order to visualise the imports graph, we write the following command:

```bash
mmap showgraph imports
```

The terminal's answer is:

```bash
Before cleaning :153 nodes/0 edges
After cleaning :0 nodes/0 edges
Rendered with pyvis
Output written to nek5000_imports.html
```

When opening *nek5000_imports.html*, we obtain the following graph where we can easily navigate in the software's imports architecture. 

![importsgraph](images/nek5000_imports.html)
(empty)
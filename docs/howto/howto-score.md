
## 3: How to use mmap score (real case)

In this how-to, we perform the Marauder's map *score* on a real case example, Nek5000, which is a fast and scalable open source CFD solver. Then we generate the associated figure being a *treefunc* with the scores added to it, and show useful information out of it.

This how-to should help you perform *mmap score* on your own case and figure out what useful information you can get from it, and how.


### Step One: Setting up and performing mmap score

See the tutorial for details on how to use the *mmap score*. Nek5000 is written in Fortran, so we create a rules file adapted to this coding language:

```Bash
mmap regexp-input fortran
```

This first command creates the pre-filled *fortran_rc_default.yml* file that needs to be edited. It contains all the regexp rules that will be used as criteria to evaluate our software's code writing. We can edit the rules if needed, and then *mmap score* can be run.
*mmap score* automatically searches for the *tree_func.json file* created before using *mmap treefunc*, so we only need to give it the rulesfile. 

```Bash
mmap score fortran_rc_default.yml
```

Now, each routine of the software has been assigned a score. We get four different results:

- For each routine, it gives (if there are some) the number of occurrences of each of the regexp errors defined in *fortran_rc_default.yml*. Here's what we get:

```Bash
DEBUG -

 core/mvmesh.f:quickmv3d
 Structure errors:
{'size': 1}


DEBUG -

 core/mvmesh.f:quickmv3d
 Regexp errors:
{'Except for indentation, single spaces are sufficient': 14,
 'Missing space after "end"': 2,
 'Missing space after first parenthesis': 3,
 'Missing space after punctuation': 88,
 'Missing space around "="': 10,
 'Missing space around operator': 2,
 'Missing space before last parenthesis': 3,
 'Missing space between subroutine name and parenthesis': 3,
 'Trailing whitespaces': 1,
 'include is discouraged, prefer use.': 3}
```

- For each routine, it gives the name and total score, between -inf and 10. For example here:

```Bash
INFO - 
core/mvmesh.f:quickmv3d: score = -10.9375
```

- Over all routines, it gives the total number of occurrences of each of the regexp errors and structure errors defined in fortran_rc_default.yml.

```Bash
INFO -  Summary
 Regexp errors:
[('Missing space after punctuation', 93788),
 ('Except for indentation, single spaces are sufficient', 22610),
 ('Missing space around "="', 9344),
 ('Intrinsics keywords should be lowercased', 7471),
 ('Missing space after first parenthesis', 5510),
 ('Missing space before last parenthesis', 5334),
 ('Missing space around operator', 5025),
 ('Missing space between subroutine name and parenthesis', 3880),
 ('include is discouraged, prefer use.', 2797),
 ('Useless ";" at end of line', 2674),
 ('Trailing whitespaces', 2277),
 ('Missing space after "end"', 2113),
 ('Missing space before parenthesis', 1319),
 ('Exactly one space after comment', 624),
 ('At least one space before comment', 425),
 ('Missing space before "="', 398),
 ('Should use 2 spaces instead of tabulation', 341),
 ('Missing space before operator', 262),
 ('goto is hard to maintain, prone to spaghetti code.', 214),
 ('Missing space after "="', 183),
 ('Missing space after operator', 182),
 ('Intrinsics named argument should be lowercased', 86),
 ('Should use "use mpi_f08" instead (or "use mpi" if not available)', 55),
 ('Bare end statement not recommended', 30),
 ('exit is an extension and should be avoided', 27),
 ('double precision is discouraged; use real instead.', 15),
 ('Use AVBP working precision', 7),
 ('Missing space around separator', 5),
 ('Missing space before separator', 4),
 ('Use strl or shortstrl', 3),
 ('You should use "[]" instead', 3),
 ('Missing space after separator', 2),
 ('pause is discouraged.', 2),
 ('Bare stop statement not recommended', 1)]


INFO -  Summary
 Structure errors:
[('argsize_belowmin', 4241),
 ('size', 539),
 ('args', 513),
 ('argsize_overmax', 49)]
```

- Finally, it gives a global score of the analyzed software. For example here:

```bash
INFO -  Summary
 Global score = -18.61335539045537
```

The output is the same *treefunc* as the input, the scores simply have been added to it. 

### Step Two: Showing treefunc with the score

We write the following command on the terminal:

```bash
mmap showtree score -f mmap_in.yml
```

We obtain the following figure, where we can navigate and see the scores.


![functree_score](images/Nek5000_functree_score.png)
## 2: How to use mmap callgraph (real case)

In this how-to, we perform the Marauder's map *callgraph* on a real case example, Nek5000, which is a fast and scalable open source CFD solver. Then we generate the associated *calls graph* and show useful information out of it. 

This how-to should help you perform Marauder's map *callgraph* and *showgraph* on your own case and figure out what useful information you can get from it, and how.


### Step One: performing mmap callgraph

See the tutorial for details on how to use the Marauder’s map *callgraph*. 

Write the following command on the terminal: 

```Bash
mmap callgraph
```

The terminal's answer is the following:

```Bash
INFO - Callgraph generated
DiGraph with 342 nodes and 273 edges
Generating nek5000/callgraph.json.
```

The data is in the callgraph.json file. Now let's show it as a calls graph. 

### Step Two: Showing the calls graph

In order to visualise the calls graph, we write the following command:

```bash
mmap showgraph callgraph
```

The terminal's answer is:

```bash
Before cleaning :342 nodes/273 edges
After cleaning :186 nodes/235 edges
Rendered with pyvis
Output written to nek5000_callgraph.html
```

When opening *nek5000_callgraph.html*, we obtain the following graph where we can easily navigate in the software's calling architecture. 

![callgraph](images/nek5000_callgraph.html)
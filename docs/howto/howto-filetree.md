## 2: How to use mmap treefile (real case)

In this how-to, we perform the Marauder's map *treefile* on a real case example, Nek5000, which is a fast and scalable open source CFD solver. Then we generate the associated *treefile* figure and show useful information out of it. 

This how-to should help you perform Marauder's map *treefile* on your own case and figure out what useful information you can get from it, and how.


### Step One: performing mmap treefile

See the tutorial for details on how to use the Marauder’s map *treefile*. 
The mmap_in.yml input file should be set up as follows (with the right path and package name): 

```Bash
# the path where your sources are stored
path : /Users/desplats/TEST/flinter/Gitlab/Nek5000/core/
# name of the package
package: Nek5000

# blacklist of unwanted nodes in the graph
remove_patterns :
  - "matplotlib*"
  - "numpy*"
  - "tkinter*"
  - "json*"
  - "PIL*"

soften_patterns :
  - "*BaseForm*"

```

Once this is done, we write the following command on the terminal: 

```Bash
mmap treefile
```

The terminal's answer is the following:

```Bash
Generating Nek5000 / file_tree.json
```

The *treefile*'s data is in the json file. Now let's show it as a tree architecture. 

### Step Two: Showing the treefile graph

In order to visualise the *treefile*, we write the following command:

```bash
mmap showtree file_complexity -f mmap_in.yml
```

We obtain the following figure, where we can easily navigate in the software's files architecture. 

![filetree_ccn](images/Nek5000_filetree_ccn.png)

This *treefile* shows the software's folders and files architecture. *mmap treefile* stops at files, unlike *mmap treefunc* which shows the different functions inside the files as well. *mmap treefile* thus allows to have an overall vision.

To show the *treefile* colored by size, we write the following command:

```bash
mmap showtree file_size -f mmap_in.yml
```

And we obtain this figure:

![filetree_size](images/Nek5000_filetree_size.png)
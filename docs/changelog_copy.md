# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).

## [0.0.0] 2022 / 09 /01

### Added
- tree graph (goes to functions level within files)
- tree graph (mmap tree --out pyvis) dumps a .html file with nodes
- tree graph (mmap tree) is a circular package view of repo structure
- tree graph (mmap tree --macro) stop at file lvl
- implemented fortran callgraph
- implemented nobvisual visualization for tree graph


### Changed

[ - ]

### Fixed

[ - ]

### Deprecated

[ - ]






Tutorials
=========

These are the fastest guides to learn about this package.
See how you can use it without thinking too much about it.

.. toctree::
    :maxdepth: 2

    ./tutorials/tuto-score


How-to Guides
=============

These are the step-by-step guides to see a practical usage of this package.
See what you can get out of it in a nominal situation.

.. toctree::
    :maxdepth: 2

    ./howto/full-analysis

    
References
==========

This is the exhaustive api description ressource.
Dive inthere to learn how to use one the function of the package.

.. toctree::
    :maxdepth: 2

    ./api/maraudersmap

Explanations
============

This is where you will find some in-depth explanation.
If your question starts with "why ?", the answer is probaly here

.. toctree::
    :maxdepth: 2

    ./explanations/expl
    

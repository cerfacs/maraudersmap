## 2: Perform mmap treefile in easy steps

This small tutorial will guide you through the different steps to complete a successful Marauder's map *treefile* and generating the associated figure.
For our example, we will perform Marauder’s map *treefile* on the folder of tests *test_macro_tree_fortran* available here: 

------------------------------------- LINK HERE -----------------------------------
https://gitlab.com/cerfacs/maraudersmap/-/tree/main/tests/func

### Pre-requirements

You have already generated the file *mmap_in.yml*. Set the parameters (path, package name). 
*treefile* will search the *-f mmap_in.yml* file in the current folder, so make sure it's there.

By default, the coloration is done by pattern. You will be able to chose an option to color by cyclomatic complexity or size if you want to. If you plan on using a coloration by pattern (for example to target one file in the tree or visualise differents blocks in the software), you need to define the patterns and their colors in the *mmap_in.yml* input file.


### Step One: Running mmap treefile


type the following command on the terminal:

```bash
mmap treefile
```

This creates a .json file with your macro-tree's data: *funs_macro_tree.json*. The terminal answers as follows:

```bash
INFO - Get tree in path : /Users/desplats/Gitlab/maraudersmap/tests/func/test_macro_tree_fortran
INFO -    root folder : test_macro_tree_fortran
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_macro_tree_fortran/binary in utf-8
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_macro_tree_fortran/modules/monte_carlo_module.mod in utf-8
INFO - Tree graph built
INFO - Generating treefile ...
Generating test / file_tree.json.
```

### Step Two: visualising the treefile

Now that you have the treefile, you can visualize it! Type the following command on the terminal, and choose an option between "file_size", "file_complexity" or "file_patterns" in order to perform an associated coloration. By default, coloration is performed by patterns. If you use this coloration, you need to have the patterns and associated colors set up in the *mmap_in.yml* input file beforehand. Here, we perform a coloration by complexity:

```bash
mmap showtree file_complexity -f mmap_in.yml
```

It shows the figure, where you can navigate and see the complexities and sizes of the different files.

![test_filetree_ccn](images/test_filetree_ccn.png)

When performing a coloration by size, we get:

![test_filetree_size](images/test_filetree_size.png)
## 1: perform mmap treefunc in easy steps


This small tutorial will guide you through the different steps to complete a successful Marauder's map *treefunc* and generating the associated figure.
For our example, we will perform Marauder’s map *treefunc* on the folder of *test_tree_fortran* available here: 

------------------------------------- LINK HERE -----------------------------------
https://gitlab.com/cerfacs/maraudersmap/-/tree/main/tests/func

### Pre-requirements

You have already generated the file *mmap_in.yml*. In this input file, set the parameters (path, package name). 
*treefunc* will search the *-f mmap_in.yml* file in the current folder, so make sure it's there.

By default, the coloration is done by pattern. You will be able to chose an option to color by cyclomatic complexity or size if you want to. If you plan on using a coloration by pattern (for example to target one file in the tree or visualise differents blocks in the software), you need to define the patterns and their colors in the *mmap_in.yml* input file.

### Step One: Running mmap treefunc


type the following command on the terminal:

```bash
mmap treefunc
```

This creates a .json file with your tree's data: *func_tree.json* (here, "func" is for "functions" since *treefunc* shows the functions inside the files, whereas *treefile* does not). The terminal answers as follows:

```bash
INFO - Get tree in path : /Users/desplats/Gitlab/maraudersmap/tests/func/test_tree_fortran
INFO -    root folder : test_tree_fortran
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_tree_fortran/binary in utf-8
Generating test / func_tree.json.
```

### Step Two: visualising treefunc

Now that you have the treefunc, you can visualise it! Type the following command on the terminal, chosing your option in "complexity", "score", "size" or "patterns".
If you choose to color by "patterns", you need to have customized the patterns and associated colors desired beforehand, in the input file *mmap_in.yml*. Here, we color by complexity.

```bash
mmap showtree complexity -f mmap_in.yml
```

It shows the figure, where you can navigate and see the complexities and sizes of the different functions and files.


![test_treefile_ccn](images/test_filetree_ccn.png)

A coloration by size gives the following result:

![test_treefile_size](images/test_filetree_size.png)
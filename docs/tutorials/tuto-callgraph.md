## 1: perform mmap callgraph in easy steps


This small tutorial will guide you through the different steps to complete a successful Marauder's map *callgraph* and generating the associated figure.
For our example, we will perform Marauder’s map *callgraph* on the test_callgraph_fortran folder of tests available here: 

------------------------------------- LINK HERE -----------------------------------
https://gitlab.com/cerfacs/maraudersmap/-/tree/main/tests/func

### Pre-requirements

You have already generated the file *mmap_in.yml* and set the parameters (path, package name). *mmap callgraph* performs a coloration by patterns, so you have set up the desired patterns and associated colors in this input file beforehand. You also need to have done *mmap treefile* and *mmap imports* before, see the precedent tutorials.


### Step One: Running mmap callgraph


type the following command on the terminal:

```bash
mmap callgraph
```

This creates a .json file with your imports tree's data: *callgraph.json*.
The terminal answers as follows:

```Bash
INFO - Killswitch engaged, calls from external packages won't be added.
INFO - Computing callgraph, this can take a while...
INFO - Callgraph generated
DiGraph with 5 nodes and 0 edges
Generating test/callgraph.json.
```

### Step Two: showing callgraph

You can now visualize the calls graph! Type the following command on the terminal:

```bash
mmap showgraph callgraph
```

The terminal's answer is:

```bash
Before cleaning :5 nodes/0 edges
After cleaning :0 nodes/0 edges
Rendered with pyvis
Output written to test_callgraph.html
```

Now type:
```bash
open test_callgraph.html
```

The command shows the calls graph figure, where you can navigate. In our case, the callgraph is empty (0 nodes and 0 edges after cleaning).

[callgraph](images/test_callgraph.html)

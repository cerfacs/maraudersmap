## 1: perform mmap imports in easy steps

This small tutorial will guide you through the different steps to complete a successful Marauder's map *imports* and generating the associated *imports graph*.
For our example, we will perform Marauder’s map *imports* on the test_immports_fortran folder of tests available here: 

------------------------------------- LINK HERE -----------------------------------
https://gitlab.com/cerfacs/maraudersmap/-/tree/main/tests/func

### Pre-requirements

You have already generated the file *mmap_in.yml*. Set the parameters (path, package name). 
*imports* will search the *-f mmap_in.yml* file in the current folder, so make sure it's there.

*mmap showgraph imports* performs the coloration by patterns. This can be used for example to target one file in the tree, or visualise differents working blocks in the software. To do this, you need to define the patterns and their colors beforehand in the *mmap_in.yml* input file.


### Step One: Running mmap imports


type the following command on the terminal:

```bash
mmap imports
```

This creates a .json file with your imports tree's data: *imports.json*. A *treefile* is generated during the routine.
The terminal answers as follows:

```Bash
INFO - Get tree in path : /Users/desplats/Gitlab/maraudersmap/tests/func/test_imports_fortran
INFO -    root folder : test_imports_fortran
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_imports_fortran/monte_carlo_module_dp.mod in utf-8
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_imports_fortran/monte_carlo_module.mod in utf-8
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_imports_fortran/monte_carlo_module_h.mod in utf-8
ERROR - Can't read /Users/desplats/Gitlab/maraudersmap/tests/func/test_imports_fortran/counts_hits_module.mod in utf-8
INFO - Tree graph built
INFO - Generating macro_tree ...
ERROR - Can't read in utf-8
ERROR - Can't read in utf-8
ERROR - Can't read in utf-8
ERROR - Can't read in utf-8
INFO - Generating imports database
INFO - Imports graph generated
DiGraph with 5 nodes and 4 edges
Generating test/imports.json.
```

### Step Two: showing imports graph

You can now visualize the imports graph! Type the following command on the terminal:

```bash
mmap showgraph imports
```

The terminal's answer is:

```bash
Before cleaning :5 nodes/4 edges
After cleaning :5 nodes/4 edges
Rendered with pyvis
Output written to test_imports.html
```

Now type:
```bash
open test_imports.html
```

It shows the import graph figure, where you can navigate.

[importsgraph](images/test_imports.html)

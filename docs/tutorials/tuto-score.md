## 3: Perform mmap score in easy steps


This small tutorial will guide you through the different steps to complete a successful Marauder's map *score* and generating the associated figure being a *treefunc* with the scores added to it.
For our example, we will perform Marauder’s map *score* on the test_tree_python in the folder of tests available here: 

------------------------------------- TESTS FOLDER -----------------------------------
https://gitlab.com/cerfacs/maraudersmap/-/tree/main/tests/func


### Pre-requirements


You have already generated the file *mmap_in.yml* and set the parameters (path and package name), and you have obtained the *mmap treefunc* in the file *func_tree.json*. Everything is automatically put in a folder named as the package and placed in your current folder.


### Step One: Setting up


First, type the following command on the terminal, with either option *python* or *fortran*, depending on what language your sources are written in. If you don't put any option, you will be in *fortran* by default. In our case, we are studying code written in Fortran.

```bash
mmap regexp-input fortran
```

This command creates a pre-filled .yml folder that needs to be edited. Indeed, the terminal answers with: 

```bash
Generating dummy regexp inputfile ./fortran_rc_default.yml for maraudersmap score.
File ./fortran_rc_default.yml created. Edit this file to customize your own rules.
```

depending on the option that you chose, the name of the rules file that was created is either *fortran_rc_default.yml* like in our case, or *python_rc_default.yml*. This file contains all the regexp rules that will be used as criteria to evaluate your sources' writing. These rules can be edited to apply better to your own case.

Here, the *fortran_rc_default* file looks like this:

```yaml

# Flinter configuation file.
# These are all the regexp rules
# Set active to false is you want to skip  rule
# All are regexp rules, meaning you can add new rules simply by editing this file
# test your rule on https://regex101.com/ if needed

extension: f\d*

regexp-rules:

  character-string-length:
    message: Use strl or shortstrl
    regexp: (?<=character)(\s*\(\s*len\s*=\s*)(?!\s*strl\b|\s*shortstrl\b)[^\(]*?(\s*\))

  missing-space-before-call-parameters:
    message: Missing space between subroutine name and parenthesis
    regexp: (?<=call\s)(\s*\w+)\(
    replacement: \1 (

  missing-space-after-call-parenthesis:
    message: Missing space after first parenthesis
    regexp: (?<=call\s)(\s*\w+\s*)\((\S)
    replacement: ( \2

 (...)

# These are rules that span over multiple lines, not accessible by regexp
# If you want to edit these rules or add your own, two options:
# - ask us.
# - fork the code.

structure-rules:

  max-statements-in-context: 50
  max-declared-locals: 12
  min-varlen: 3
  max-varlen: 20
  max-arguments: 5
  min-arglen: 3
  max-arglen: 20
  max-nesting-levels: 5
  var-declaration: '(?:{types})\s*(?:\(.*\))?\s*(?:::| )\s*([A-Za-z_]\w*(?:\s*,\s*[A-Za-z_]\w*)*)'

######################################################################################
# These are the fortran syntax we use to parse the source
# A priori there is no need to edit, but Fortran is so vast in time...
######################################################################################

syntax:
  types:
    - real
    - character
    - logical
    - integer
    - complex
    - double precision

  operators:
    - '\.eq\.'
    - '=='
    - '\.neq\.'
    - '/='
    - '\.gt\.'
    - '>'
    - '\.lt\.'
    - '<'
    - '\.geq\.'
    - '>='
    - '\.leq\.'
    - '<='
    - '\.le\.'
    - '\.ge\.'
    - '\.and\.'
    - '\.or\.'

  structs:
    - if
    - select
    - case
    - while

(...)
```

### Step Two: Running mmap score


When you're all set, type the following command on the terminal, giving to *mmap score* your edited rules file:

```bash
mmap score fortran_rc_default.yml
```

*mmap score* automatically searches for the *tree_func.json file* created before using *mmap treefunc*.
This command performs Marauder's map *score*, adding a score to each routine in the *treefunc*. Three different results come in the logging: 

- *DEBUG* : For each routine, it gives the number of occurrences of the regexp errors and structure errors found in the code, the rules being defined in *python_rc_default.yml* or *fortran_rc_default.yml*.


- *INFO* : For each routine, it gives a total score, between 0 and 10.


- *INFO - Summary* : Over all routines, it gives the total number of occurrences of each of the regexp errors and structure errors defined in *python_rc_default.yml* or *fortran_rc_default.yml*.
  
- *INFO - Summary, Global score* : Finally, it gives a global score of the analyzed software.



In our example, the terminal gives: 

```bash 
INFO -
 test_tree_fortran/dummy.yml: score = 10.0

INFO -
 test_tree_fortran/estimate_pi.f90:__script__: score = 7.0

DEBUG -
 test_tree_fortran/estimate_pi.f90:__script__
 Regexp errors:
{'Missing space after first parenthesis': 1,
 'Missing space before last parenthesis': 1,
 'Missing space between subroutine name and parenthesis': 1}

INFO -
 test_tree_fortran/modules/monte_carlo_module.f90:__script__: score = 5.0

DEBUG -
 test_tree_fortran/modules/monte_carlo_module.f90:__script__
 Regexp errors:
{'Except for indentation, single spaces are sufficient': 1,
 'Missing space after first parenthesis': 3,
 'Missing space after punctuation': 2,
 'Missing space before last parenthesis': 3,
 'Missing space between subroutine name and parenthesis': 3,
 'Use AVBP working precision': 2}

INFO -
 test_tree_fortran/funsub.f90:__script__: score = 0

DEBUG -
 test_tree_fortran/funsub.f90:__script__
 Structure errors:
{'argsize_belowmin': 1}

DEBUG -
 test_tree_fortran/funsub.f90:__script__
 Regexp errors:
{'Except for indentation, single spaces are sufficient': 2,
 'Intrinsics keywords should be lowercased': 44,
 'Missing space after punctuation': 19}

INFO -  Summary
 Regexp errors:
[('Intrinsics keywords should be lowercased', 44),
 ('Missing space after punctuation', 21),
 ('Missing space between subroutine name and parenthesis', 4),
 ('Missing space after first parenthesis', 4),
 ('Missing space before last parenthesis', 4),
 ('Except for indentation, single spaces are sufficient', 3),
 ('Use AVBP working precision', 2)]

INFO -  Summary
 Structure errors:
[('argsize_belowmin', 1)]

INFO -  Summary
 Global score = 2.7160493827160495
Generating test/func_tree_score.json with scores.
```

### Step Three: Showing treefunc with scores

The output of *mmap score* is a file named *func_tree_score.json* which is the same file as the *func_tree.json* taken for input, with the scores added to its data. 
To visualise it, use the following command:

```bash
mmap showtree score -f mmap_in.yml
```

It shows the figure, where you can navigate and see the scores. Here:


![test_tree](images/test_functree_score.png)
## Explanations: get started with mmap

How does it work ?

### 1 - Setup : mmap anew

```Bash
mmap anew
```

This command creates an import file, called *mmap_in.yml* which needs to be edited to precise the path pointing to the sources, the package name, and the coloration rule. 

The parameter "color_by" can be set to either: 
- ccn (the tree graphs will be colored by complexity)
- size (the tree graphs will be colored by size)
- rules (the tree graphs will be colored by rules)
  
Choosing the last option means that the "color_rules" have to be edited too.


### 2 - Using mmap to analyse a software


The following steps that you will follow are detailed in the *Tutorials* sections with simple examples to get you started, and real examples will be taken in the *How to* sections. 

In order, we have:

- *mmap tree*, or *mmap macro-tree* : these 2 kinds of tree show the folders architecture of the code. *mmap tree* shows the whole architecture with the various functions inside the files, whereas *mmap macro-tree* stops at the files to get a more overall vision. Both *mmap tree* and *mmap macro-tree* need the input file mmap_in.yml to be created and set up beforehand, and they return a .json file with the data inside. To visualise the obtained tree, you will use *mmap show*.

![macrotree-ccn](../howto/images/macrotree-ccn.png) 
*This is the macro-tree graph colored by ccn of Nek5000 opensource software...*

![tree-ccn](../howto/images/tree-ccn.png)
*...whereas this is the tree graph colored by ccn of Nek5000.*
  
- *mmap score* : to get a score tree, you need to have performed *mmap tree* first. You will also need to create a default rules input file, containing rules for Fortran or Python according to the coding language of your sources. This file, named *fortran_rc_default* or *python_rc_default*, is created using the command *regexp-input*. After editing it according to your needs, you will be able to perform *mmap score* by giving it the .json file of the tree and the rules file. You will then have scores given to each function of the code, according to the rules. To visualise the obtained tree, you will use *mmap show*.

![macrotree-ccn](../howto/images/scoretree.png)
*This is the score tree graph, colored by score, of Nek5000 opensource software.
"""
Unit test for tree.py file for python code
"""
import matplotlib.pyplot as plt
import networkx as nx
from maraudersmap.nx_utils import (
    get_maingraph,
    get_subgraph,
    remove_by_patterns,
    soften_by_patterns,
    remove_hyperconnect,
    remove_singles,
    soften_by_patterns,
    get_root_from_tree,
    merge_node,
)
from maraudersmap.show_pyplot import fastplot




def test_merge_node():
    """
    
    :

    1->2->3    
       v
       4

    on 2 (mergeup) gives:
    2->3    
    v
    4

    on 2 (mergedown) gives:
    1->2

    """
    graph = nx.DiGraph()
    # Add nodes to the graph
    graph.add_nodes_from([1, 2, 3, 4])
    # Add edges to form subgraph 1
    graph.add_edges_from([(1, 2), (2, 3), (2, 4)])
    
    mergeup = nx.DiGraph()
    # Add nodes to the graph
    mergeup.add_nodes_from([2, 3, 4])
    # Add edges to form subgraph 1
    mergeup.add_edges_from([(2, 3), (2, 4)])    

    mergedown = nx.DiGraph()
    # Add nodes to the graph
    mergedown.add_nodes_from([1, 2])
    # Add edges to form subgraph 1
    mergedown.add_edges_from([(1, 2)])


    res = merge_node(graph, 2, mergeup=True)
    
    #fastplot(res,"red")
    #fastplot(mergeup,"green")
    #plt.show()
    #assert 0==1
    
    
    assert nx.is_isomorphic(mergeup,res)
    res = merge_node(graph, 2, mergeup=False)
    assert nx.is_isomorphic(mergedown,res)


    
def test_get_maingraph():
    """
    :

    1->2->3          5->6->7
       v
       4

    gives

    1->2->3 
       v
       4
    """
    # Create an empty directed graph
    graph = nx.DiGraph()
    # Add nodes to the graph
    graph.add_nodes_from([1, 2, 3, 4])
    # Add edges to form subgraph 1
    graph.add_edges_from([(1, 2), (2, 3), (2, 4)])
    s1 = graph.copy()
    graph.add_nodes_from([5, 6, 7])
    # Add edges to form subgraph 2
    graph.add_edges_from([(5, 6), (6, 7)])
    res = get_maingraph(graph)
    assert nx.is_isomorphic(s1,res)


def test_get_subgraph():
    """
    :

    a1->a2->a3          b5->b6->b7
       v
       a4

    with "b6" gives

    b5->b6->b7
    """
    # Create an empty directed graph
    graph = nx.DiGraph()
    # Add nodes to the graph
    graph.add_nodes_from([ "b6", "b7"])
    # Add edges to form subgraph 2
    graph.add_edges_from([ ("b6", "b7")])
    
    graph.add_nodes_from([ "b5"])
    # Add edges to form subgraph 2
    graph.add_edges_from([ ("b5", "b6")])
    
    s1 = graph.copy()
   
    graph.add_nodes_from(["a1", "a2", "a3", "a4"])
    # Add edges to form subgraph 1
    graph.add_edges_from([("a1", "a2"), ("a2", "a3"), ("a2", "a4")])
    res = get_subgraph(graph, "b6")
    assert nx.is_isomorphic(s1,res)

def test_remove_by_patterns():
    """
    :

    a1->a2->a3          b5->b6->b7
       v
       a4

   
    Case 1
    with ["a*", "b6"] gives

    b5 b7

    Case 2
    with ["a*", "b6"] and remove descendants gives

    b5

        """
    out1 = nx.DiGraph()
    out1.add_nodes_from([ "b5", "b6"])
    
    # Add nodes to the graph
    out2 = nx.DiGraph()
    out2.add_nodes_from([ "b5"])
    
    
    # Add edges to form subgraph 2
    origin = nx.DiGraph()
    origin.add_nodes_from(["a1", "a2", "a3", "a4","b5","b6","b7"])
    origin.add_edges_from([("a1", "a2"), ("a2", "a3"), ("a2", "a4")])
    origin.add_edges_from([("b5", "b6"), ("b6", "b7")])
    
    test1 = remove_by_patterns(origin, ["a*", "b6"])
    test2 = remove_by_patterns(origin, ["a*", "b6"],remove_descendants=True )
    assert nx.is_isomorphic(out1,test1)

    assert nx.is_isomorphic(out2,test2)


def test_remove_hyperconnect():
    """
    :
       a4
       v
    a2->a1<-a3          b5->b6->b7

    with thresold 3 gives

    b5->b6->b7    a2 a3 a4
    """
    # Create an empty directed graph
    graph = nx.DiGraph()
    # Add nodes to the graph
    graph.add_nodes_from([ "b5", "b6", "b7"])
    # Add edges to form subgraph 2
    graph.add_edges_from([ ("b5", "b6"),("b6", "b7")])
    graph.add_nodes_from(["a2", "a3", "a4"])


    s1 = graph.copy()

    graph.add_nodes_from(["a1"])
    # Add edges to form subgraph 1
    graph.add_edges_from([("a2", "a1"), ("a3", "a1"), ("a4", "a1")])
    res = remove_hyperconnect(graph, treshold=3)
    assert nx.is_isomorphic(s1,res)


def test_remove_singles():
    """
    :

     a1->a2->a3      
       v
       a4
               b1    (b2>b2)

    here b2 is connected to itself alone
    
    gives
     a1->a2->a3      
       v
       a4
    """
    # Create an empty directed graph
    graph = nx.DiGraph()
    # Add nodes to the graph
    graph.add_nodes_from(["a1", "a2", "a3", "a4"])
    # Add edges to form subgraph 1
    graph.add_edges_from([("a1", "a2"), ("a2", "a3"), ("a2", "a4")])
    

    s1 = graph.copy()

    graph.add_nodes_from(["b1", "b2"])
    graph.add_edges_from([("b2", "b2")])
    res = remove_singles(graph)
    assert nx.is_isomorphic(s1,res)



def test_soften_by_patterns():
    """
    add a true soften attribute to nodes by patterns
    """
    # Create an empty directed graph
    graph = nx.DiGraph()
    # Add nodes to the graph
    graph.add_nodes_from(["a1", "a2", "a3", "a4"])
    # Add edges to form subgraph 1
    graph.add_edges_from([("a1", "a2"), ("a2", "a3"), ("a2", "a4")])
    res = soften_by_patterns(graph, ["a*", "b5"])
    assert res.nodes["a1"]["soften"] == True
    


def test_get_root_from_tree():
    """
    add a true soften attribute to nodes by patterns


    a1->a2->a3      
       v
       a4
    
       returns a1

     a1->a2->a3     b1->b2->b3      
       v               v
       a4              b4
    
       returns failure

    a1->a2<-a3      
       v
       a4
    
       returns failure


       
        """
    # Create an empty directed graph
    graph = nx.DiGraph()
    # Add nodes to the graph
        # Add nodes to the graph
    graph.add_nodes_from(["a1", "a2", "a3", "a4"])
    # Add edges to form subgraph 1
    graph.add_edges_from([("a1", "a2"), ("a2", "a3"), ("a2", "a4")])

    # success if single tree===========
    root = get_root_from_tree(graph)
    assert root == "a1"
    
    # fail if multiple tree==========
    graph.add_nodes_from(["b1", "b2", "b3", "b4"])
    # Add edges to form subgraph 1
    graph.add_edges_from([("b1", "b2"), ("b2", "b3"), ("b2", "b4")])
    exception = False
    try :
        get_root_from_tree(graph)
    except ValueError:
        exception = True
    assert exception

    # Fail if multiple root =================
    graph2 = nx.DiGraph()
    # Add nodes to the graph
        # Add nodes to the graph
    graph2.add_nodes_from(["a1", "a2", "a3", "a4"])
    # Add edges to form subgraph 1
    graph2.add_edges_from([("a1", "a2"), ("a2", "a3"), ("a4", "a2")])
    
    
    exception = False
    try :
        get_root_from_tree(graph2)
    except ValueError:
        exception = True
    assert exception
    


from maraudersmap.callgraph import end_match, find_function_reference

def test_end_match():

    data = [
        "dummy.f90:func1",
        "dummy.f90:func2",
        "dummy.f90:obj1.meth1",
        "dummy.f90:obj1.meth2",
        "dummy.f90:obj2.meth1",
        "dumbass.f90:obj2.meth1",
    ]

    assert end_match(data, "foobar") == []    # no match
    assert end_match(data, "func2") == ["dummy.f90:func2"] # single match
    assert end_match(data, "th2") == []     # No match out of dots
    assert end_match(data, "meth1") == ["dummy.f90:obj1.meth1", "dummy.f90:obj2.meth1","dumbass.f90:obj2.meth1"] # multiple match
    assert end_match(data, "any.meth2", rootless=True) == ["dummy.f90:obj1.meth2"] #rootless > remove any.
    assert end_match(data, "any.func1", rootless=True) == [] #rootless > remove any. but no match if tager not an object.
    
def test_find_function_reference():

    data = [
        "dummy.f90:func1",
        "dummy.f90:func2",
        "dummy.f90:obj1.meth1",
        "dummy.f90:obj1.meth2",
        "dummy.f90:obj2.meth1",
        "dumbass.f90:obj2.meth1",
    ]

    assert find_function_reference(data, "foobar", "dummy") == (None, None)    # no match
    assert find_function_reference(data, "func2", "dummy") == ("func2","dummy.f90")  
    assert find_function_reference(data, "func2", "garbage") == ("func2","dummy.f90")  # single matc
    assert find_function_reference(data, "th2", "dummy") == (None, None) 
    
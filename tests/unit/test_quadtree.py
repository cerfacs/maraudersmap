import numpy as np
from maraudersmap.quadtree import QuadTree


def test_quadtree():
    """
    Test with the following configuration

     1|
      |          12
      |          34
     0|
      |   
      |   0
      |
    -1|---------------
    -1       0       1     
    
    Qds elements ordering, useful to understand .content()
    0 1
    3 2
    
    """
    x= [-0.5 , 0.4 , 0.6 , 0.4, 0.6]
    y= [-0.5 , 0.6 , 0.6 , 0.4, 0.4]
    coords = np.array([x,y]).T
    assert coords.shape ==(5,2)
    qtr4 = QuadTree(
        coords=coords,
        max_in_quad=4
    )

    qtr2 = QuadTree(
        coords=coords,
        max_in_quad=2
    )

    qtr4med = QuadTree(
        coords=coords,
        max_in_quad=4,
        median_splitting=True
    )
    #print([quad.ids for quad in qtr4.quad_list()])
    # QuadTree.content gives the nested list of elemnts by ids
    assert qtr4.content() == [ [], [1,2,3,4], [], [0]]
    assert qtr2.content() == [[], [[1], [2], [4], [3]], [], [0]]
    assert qtr4med.content() == [[1], [2], [4], [0, 3]]
    
    # qtr2, note the 4 - 3 instead of 3 - 4 because of the order of the quads.

    # QuadTree.quad_list gives the list of nodes in  non empty Quads by ids
    assert qtr4.ids_clustered() == [[1, 2, 3, 4], [0]]
    assert qtr2.ids_clustered() == [[1], [2], [4], [3], [0]]
    assert qtr4med.ids_clustered() == [[1], [2], [4], [0, 3]]
    
        

    



    
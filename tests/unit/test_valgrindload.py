from maraudersmap.valgrindload import parse_valgrind_line, node_name,reorder_body


def test_parse_valgrind_line():
    line = " 2,863,917,060 ( 1.73%)  *  ???:get_face_norm [/home/logiciels/local_pgi/lib/libsup7_pgi.so]"
    assert parse_valgrind_line(line) == (
        2863917060,
        1.73,
        "*",
        "???",
        "get_face_norm",
        1,
        "/home/logiciels/local_pgi/lib/libsup7_pgi.so",
    )

    line = " 62,692 ( 0.00%)  >   /scratch/coop/ghenai/avbp_openacc/avbp/BUILD_KRAKEN_A100/../SOURCES/COMMON/mod_alloc.f90:mod_alloc_dealloc_int_2d_ (6x) [/scratch/coop/ghenai/avbp_openacc/avbp/HOST/KRAKEN_A100/BIN/AVBP_V7_dev.KRAKEN_A100]"
    assert parse_valgrind_line(line) == (
        62692,
        0.00,
        ">",
        "/scratch/coop/ghenai/avbp_openacc/avbp/BUILD_KRAKEN_A100/../SOURCES/COMMON/mod_alloc.f90",
        "mod_alloc_dealloc_int_2d_",
        6,
        "/scratch/coop/ghenai/avbp_openacc/avbp/HOST/KRAKEN_A100/BIN/AVBP_V7_dev.KRAKEN_A100",
    )

    line = "165,287,087,706 (99.94%)  *  ???:(below main) [/usr/lib64/libc-2.17.so]"
    assert parse_valgrind_line(line) == (
        165287087706,
        99.94 ,
        "*",
        "???",
        "below_main",
        1,
        "/usr/lib64/libc-2.17.so",
    )
    line = "165,285,857,316 (99.94%)  < ???:(below main) (6x) [/usr/lib64/libc-2.17.so]"
    assert parse_valgrind_line(line) == (
        165285857316,
        99.94 ,
        "<",
        "???",
        "below_main",
        6,
        "/usr/lib64/libc-2.17.so",
    )
    
# print(parse_valgrind_line(" 62,692 ( 0.00%)  >   /scratch/coop/ghenai/avbp_openacc/avbp/BUILD_KRAKEN_A100/../SOURCES/COMMON/mod_alloc.f90:mod_alloc_dealloc_int_2d_ (6x) [/scratch/coop/ghenai/avbp_openacc/avbp/HOST/KRAKEN_A100/BIN/AVBP_V7_dev.KRAKEN_A100]"))

def test_reorder_body():

    body="""
2,863,917,060 ( 1.73%)  <  foo:func bar
2,863,917,060 ( 1.73%)  <  foo:func bar
2,863,917,060 ( 1.73%)  *  foo:func bar
2,863,917,060 ( 1.73%)  >  foo:func bar
2,863,917,060 ( 1.73%)  >  foo:func bar

2,863,917,060 ( 1.73%)  *  fu:func bar
2,863,917,060 ( 1.73%)  >  fu:func bar
2,863,917,060 ( 1.73%)  >  fu:func bar

2,863,917,060 ( 1.73%)  < rofl:func copter
2,863,917,060 ( 1.73%)  <  rofl:func copter
2,863,917,060 ( 1.73%)  *  rofl:func copter

""".splitlines()
    ref_body_reordered="""2,863,917,060 ( 1.73%)  *  foo:func bar
2,863,917,060 ( 1.73%)  <  foo:func bar
2,863,917,060 ( 1.73%)  <  foo:func bar
2,863,917,060 ( 1.73%)  >  foo:func bar
2,863,917,060 ( 1.73%)  >  foo:func bar
2,863,917,060 ( 1.73%)  *  fu:func bar
2,863,917,060 ( 1.73%)  >  fu:func bar
2,863,917,060 ( 1.73%)  >  fu:func bar
2,863,917,060 ( 1.73%)  *  rofl:func copter
2,863,917,060 ( 1.73%)  < rofl:func copter
2,863,917,060 ( 1.73%)  <  rofl:func copter
""".splitlines()
    test_body_reordered = reorder_body(body)
    print("\n".join(test_body_reordered))
    print("===")
    print("\n".join(ref_body_reordered))
    assert test_body_reordered == ref_body_reordered

def test_node_name():
    
    assert node_name("prefix/file","name","prefix/") == "file:name"

    assert node_name("prefix/file","name","random") == "!!!prefix/file:name"

    assert node_name("prefix/file","name","") == "prefix/file:name"
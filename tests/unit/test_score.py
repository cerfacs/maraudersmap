"""
Unit test for tree.py file for python code
"""

import os
from yaml import safe_load
import networkx as nx
from maraudersmap.score import (
    compute_rate,
    #chaining,
    compute_struct_items,
    rate_structure_errors
)

SYNTAX= {
    'context_blocks': ['function'],
    'intrinsics': ['ALLOCATABLE', 'ALLOCATE'],
    'named-args': ['ACCESS', 'ACTION'],
    'namespace_blocks': ['program'],
    'operators': ['<', '>=', '<='],
    'punctuations': [',', ';'],
    'structs': ['if','do'],
    'types': ['real', 'integer']
}

JOINED_SYNTAX = {
    '{alltypes_lower}': 'real|integer',
    '{alltypes_upper}': 'REAL|INTEGER',
    '{alltypes}': 'REAL|INTEGER|real|integer',
    '{blocks_lower}': 'program|function',
    '{blocks_upper}': 'PROGRAM|FUNCTION',
    '{blocks}': 'PROGRAM|FUNCTION|program|function',
    '{intrinsics_lower}': 'allocatable|allocate',
    '{intrinsics_upper}': 'ALLOCATABLE|ALLOCATE',
    '{named-args_lower}': 'access|action',
    '{named-args_upper}': 'ACCESS|ACTION',
    '{named-args}': 'access|action',
    '{operators}': '<|>=|<=',
    '{punctuations}': ',|;',
    '{structs_lower}': 'if|do',
    '{structs_upper}': 'IF|DO',
    '{structs}': 'IF|DO|if|do'
}

def test_compute_rate():
    """Testing creation of lizard database"""
    
    assert compute_rate(10,5,5) == -20

# Not working
# def test_chaining(datadir):
#     """Testing creation of lizard database"""
#     # file = os.path.join(datadir, "fortran_test.yml")
#     # with open(file,"r") as fin:
#     #     rules = safe_load(fin)
#     # print(rules["syntax"])
#     # assert 0 == 1

#     assert JOINED_SYNTAX == chaining(SYNTAX)

def test_compute_struct_items():

    code = """
FUNCTION HELLO (a, good, azertyuiopqsdfghjklmwxcvbn)
    implicit none
    ! comment
    INTEGER :: a, good, azertyuiopqsdfghjklmwxcvbn
    INTEGER :: n_points
    REAL :: pi_estimate, e
    INTEGER :: i, j, k
    REAL :: x

    n_points = 1000000
    pi_estimate = 3.14
    e = 2.16
    
    do i = 1, 5
        DO j = 1, 3
            do k = 1, 2
                if (k >= 2) then
                    ! Perform actions inside the nested loop
                else
                    ! Perform other actions
                end if
                print *, i, j, k
            end do
        end do
    end do

    x = 0.0
    azertyuiopqsdfghjklmwxcvbn = 123456
    x = sin(abs(0.0))

END FUNCTION HELLO
""".split("\n")

    result =  {
        'size': 33,
        'list_args': ['a', 'good', 'azertyuiopqsdfghjklmwxcvbn'], 
        'list_vars': ['azertyuiopqsdfghjklmwxcvbn', 'e','i','j','k', 'n_points', 'pi_estimate', 'x'], 
        'nesting_level': 5
    }
    assert  compute_struct_items(code) == result


def test_rate_structure_errors():

    struct_items =  {
        'size': 11,
        'list_args': ['a', 'good', 'azertyuiopqsdfghjklmwxcvbn'], 
        'list_vars': ['azertyuiopqsdfghjklmwxcvbn', 'e', 'n_points', 'pi_estimate', 'x'], 
        'nesting_level': 8
    }
    struct_rules= {
        "max-statements-in-context": 3, # artificially small to test the rating
        "max-declared-locals": 2, # artificially small to test the rating
        "min-varlen": 3,
        "max-varlen": 20,
        "max-arguments": 1, # artificially small to test the rating
        "min-arglen": 3,
        "max-arglen": 20,
        "max-nesting-levels": 5,
    }

    result =  {
        'size': 4,
        'args': 3,
        'argsize_belowmin': 1,
        'argsize_overmax': 1,
        'locals': 2,
        'nesting': 3,
        'varlen_belowmin': 2,
        'varlen_overmax': 1
    }
    assert  rate_structure_errors(struct_items,struct_rules) == result
import numpy as np
from maraudersmap.layout_forces import (
    connexion_forces, 
    _apply_repulsion,
    repulsion_forces_neighbors,
    repulsion_forces_macros,
    spring_model,
    gravity_frontier,
    gravity_level_forces,
)

#import matplotlib.pyplot as plt

def test_spring_model(allclose):
    l=0.5
    distances = np.linspace(0,3*l,8)
    springs = spring_model(distances,l)
    # plt.plot(distances,springs)
    # plt.plot(distances,distances/l-1,"+")
    # plt.plot(distances,distances/2,"x")
    # plt.show()
    assert allclose(springs,np.array([-0.245     , -0.245     , -0.16666667,  0.28571429,  0.42      ,0.525     ,  0.63      ,  0.735]))



def test_force_connection(allclose):
    """
    GRAPH

    0-----1
          |
    3-----2
    
    """
    coords= np.array([
        [-1, 1],
        [1, 1],
        [1, -1],
        [-1, -1],
    ])
    conn = np.array([ [0,1],[2,3],[1,2] ])
    
    expected = np.array([
        [ 0.98,  0.  ],
        [-0.98, -0.98],
        [-0.98,  0.98],
        [ 0.98,  0.  ],
    ])
    
    motion = connexion_forces(coords,conn)
    assert allclose(motion,expected)

    motion = connexion_forces(coords,conn, length=2.)
    expected_equil = np.array([
        [ 0.0,  0.0],
        [ 0.0, 0.0],
        [ 0.0,  0.0],
        [ 0.0,  0.0],
    ])    
    assert allclose(motion,expected_equil)


    motion = connexion_forces(coords,conn, length=0.5)
    expected_pull = np.array([
        [ 0.98,  0.  ],
        [-0.98, -0.98],
        [-0.98,  0.98],
        [ 0.98,  0.  ],
    ])
    assert allclose(motion,expected_pull)

    motion = connexion_forces(coords,conn, length=4.)
    expect_push = np.array([
        [ -1.0,  0.0  ],
        [ 1.0, 1.0],
        [ 1.0,  -1.0],
        [ -1.0,  0.  ],
    ])
    
    assert allclose(motion,expect_push)


def test_apply_repulsion(allclose):
    """
        points
                A
                0         2 >
        
                  

                1
                V
    """
    coords= np.array([
        [0., 0.],
        [0., -1.],
        [1., 0.],
    ])
    expected = np.array([
        [ -1.,  1.  ],
        [- 0.59460356, -1.59460356],
        [ 1.59460356,  0.59460356],
    ])
    motion = _apply_repulsion(coords)  # most atomic repulsion
    assert allclose(motion,expected)

    motion2 = repulsion_forces_neighbors(coords,neighbors=3) # CLOSE MOTIONS
    assert allclose(motion2,expected)

    motion3 = repulsion_forces_macros(coords,max_in_quad=10)# MACRO MOTIONS , NO QUAD DIVISIONS
    assert allclose(motion3,expected)
    
    motion3bis = repulsion_forces_macros(coords,max_in_quad=1) # MACRO MOTIONS ,  QUAD DIVISIONS
    assert allclose(motion3bis,expected)




def test_gravity(allclose):
    coords= np.array([
        [0., 0.],
        [0., -2.],
        [2., 0.],
    ])
    expected = np.array([
        [ 0,  0.  ],
        [ 0.0, 1.0],
        [ -1.0, 0.0],
    ])
    motion = gravity_frontier(coords=coords,rim=4., expnt=2)
    assert allclose(motion,expected)

    expected = np.array([
        [ 0,  0.  ],
        [ 0.0, 2.0],
        [ -2.0, 0.0],
    ])
    motion = gravity_frontier(coords=coords,rim=4., expnt=1)
    assert allclose(motion,expected)

    coords= np.array([
        [0., 0.],
        [0., -4.],
        [4., 0.],
    ])
    expected = np.array([
        [ 0,  0.  ],
        [ 0.0, 2.0],
        [ -2.0, 0.0],
    ])
    motion = gravity_frontier(coords=coords,rim=8., expnt=2)
   
   
    assert allclose(motion,expected)

def test_gravity_level_forces(allclose):
    coords= np.array([
        [0., 0.],
        [1., 0.],
        [2., 0.],
        [3., 0.],
    ])
    depth= np.array([0, 1, 2, 3 ])
        
    expected = np.array([
        [ 0,  0.  ],
        [ 0.0,.0],
        [ -0.0, 0.0],
        [ -0.0, 0.0],
    ])
    motion = gravity_level_forces(coords,depth_array=depth,rim=3)
    assert allclose(motion,expected)

    coords= np.array([
        [0., 0.],
        [1., 0.],
        [2., 0.],
        [3., 0.],
    ])
    depth= np.array([0, 1, 2, 3 ])
        
    expected = np.array([
        [ 0,  0.  ],
        [ 1.0,.0],
        [ 2.0, 0.0],
        [ 3.0, 0.0],
    ])
    motion = gravity_level_forces(coords,depth_array=depth,rim=6)
    assert allclose(motion,expected)

    coords= np.array([
        [3., 0.],
        [3., 0.],
        [3., 0.],
        [3., 0.],        
        ])
    depth= np.array([0, 1, 2, 3 ])
        
    expected = np.array([
        [ -3,  0.  ],
        [ -2,.0],
        [ -1, 0.0],
        [ 0, 0.0],
    ])
    motion = gravity_level_forces(coords,depth_array=depth,rim=3)
    assert allclose(motion,expected)
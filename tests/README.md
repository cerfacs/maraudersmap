# Testing structure


## Functionnal tests `/func/`

For high-level features.
Tests using the datadir fixture, with a `redo.sh` in the datadir showing the CLI end use of the package.
This redo re-generate the targets of the tests , and should cancel any discrepancy if the code is changed.

# Unit tests `unit`

For low-level features.
Tests are forbidden to use the datadir fixture. They must be small enough to store their targets in the body of the test. 
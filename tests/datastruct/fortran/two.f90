
type space_t
    integer :: t               !< Space type (GL, GLL, GJ, ...)
    real(kind=rp), allocatable :: zg(:,:) !< Quadrature points
    !
    ! Device pointers (if present)
    !
    type(c_ptr) :: dr_inv_d = C_NULL_PTR
    type(c_ptr) :: ds_inv_d = C_NULL_PTR
end type space_t


type c_ptr
    integer :: i               !< Space type (GL, GLL, GJ, ...)
 
end type c_ptr
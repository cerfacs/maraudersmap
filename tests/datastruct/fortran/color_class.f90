MODULE ColorModule
    IMPLICIT NONE

    ! Define the Color class
    TYPE :: Color
        PRIVATE
        INTEGER :: red
        INTEGER :: green
        INTEGER :: blue
        CONTAINS
        PROCEDURE :: print_color => print_color_subroutine
        PROCEDURE :: set_color
    END TYPE Color

CONTAINS

    ! Method to set the RGB values of the color
    SUBROUTINE set_color(this, r, g, b)
        CLASS(Color), INTENT(INOUT) :: this
        INTEGER, INTENT(IN) :: r, g, b
        this%red = r
        this%green = g
        this%blue = b
    END SUBROUTINE set_color

    ! Subroutine to print out the RGB values of the color
    SUBROUTINE print_color_subroutine(this)
        CLASS(Color), INTENT(IN) :: this
        PRINT *, "Color (RGB):", this%red, this%green, this%blue
    END SUBROUTINE print_color_subroutine
END MODULE ColorModule

PROGRAM ColorExample
    USE ColorModule

    ! Declare a variable of the Color class
    TYPE(Color) :: my_color

    ! Set the RGB values of the color
    CALL my_color%set_color(255, 0, 0) ! Set color to red

    ! Call the method using the pointer
    CALL my_color%print_color

END PROGRAM ColorExample

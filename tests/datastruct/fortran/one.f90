



type, public :: interpolator_t
    type(space_t), pointer :: Xh
    type(space_t), pointer :: Yh
    real(kind=rp), allocatable :: Xh_to_Yh(:,:), Xh_to_YhT(:,:)
    real(kind=rp), allocatable :: Yh_to_Xh(:,:), Yh_to_XhT(:,:)
    type(c_ptr) :: Xh_Yh_d = C_NULL_PTR
    type(c_ptr) :: Xh_YhT_d = C_NULL_PTR
    type(c_ptr) :: Yh_Xh_d = C_NULL_PTR
    type(c_ptr) :: Yh_XhT_d = C_NULL_PTR

    contains
    procedure, pass(this) :: init => interp_init
    procedure, pass(this) :: free => interp_free
    procedure, pass(this) :: map => interpolate
end type interpolator_t

MODULE ColorModule
    ! Define the base color type
    TYPE :: color
        INTEGER :: red
        INTEGER :: green
        INTEGER :: blue
    END TYPE color

    ! Define the blue type
    TYPE, EXTENDS(color) :: blue
        ! Blue color doesn't have any extra attributes
    END TYPE blue

    ! Define the darkblue type
    TYPE, EXTENDS(blue) :: darkblue
        INTEGER :: darkness_level
    END TYPE darkblue

    ! Define the lightblue type
    TYPE, EXTENDS(blue) :: lightblue
        INTEGER :: brightness_level
    END TYPE lightblue

    ! Define the mix type
    TYPE :: mix
        TYPE(color) :: color1
        TYPE(color) :: color2
    END TYPE mix
END MODULE ColorModule

PROGRAM ColorExample
    USE ColorModule

    ! Declare variables of each type
    TYPE(blue) :: b1
    TYPE(darkblue) :: db1
    TYPE(lightblue) :: lb1
    TYPE(mix) :: mix1

    ! Initialize variables
    b1%red = 0
    b1%green = 0
    b1%blue = 255

    db1%red = 0
    db1%green = 0
    db1%blue = 139
    db1%darkness_level = 75

    lb1%red = 173
    lb1%green = 216
    lb1%blue = 230
    lb1%brightness_level = 85

    ! Set colors for mixing
    mix1%color1 = b1
    mix1%color2 = lb1

    ! Output information
    PRINT *, "Mix Color (Color1, Color2):", mix1%color1%red, mix1%color1%green, mix1%color1%blue, &
                                            mix1%color2%red, mix1%color2%green, mix1%color2%blue
END PROGRAM ColorExample

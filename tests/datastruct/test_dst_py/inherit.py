

class Red():
    def meth1(self, a, b):
        return a + b

    def meth2(self, a, b):
        return a - b
    

class Blue():
    def meth3(self, a, b):
        return a + b

    def meth4(self, a, b):
        return a - b
    
class Purple(Red,Blue):

    def meth1(self, a:Red, b):
        return a * b

    def meth4(self, a, b):
        return super().meth4(a, b)

# Instantiate Operation2
operation_instance = purple()



operation_instance.meth4(1,1)
# Test methods
# result_addition = operation_instance.add(5, 3)
# result_subtraction = operation_instance.subtract(5, 3)
# result_multiplication = operation_instance.multiply(5, 3)
# result_division = operation_instance.divide(5, 3)

# # Output results
# print("Addition:", result_addition)
# print("Subtraction:", result_subtraction)
# print("Multiplication:", result_multiplication)
# print("Division:", result_division)
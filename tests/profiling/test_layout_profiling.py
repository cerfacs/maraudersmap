
import numpy as np
from maraudersmap.layout_forces import (
    connexion_forces, 
    repulsion_forces_neighbors,
    repulsion_forces_macros,
)

def build_layout(ni=8,nj=8):
    coords=[]
    conn=[]
    
    def _idx(i,j,ni,nj):
        return (j+i*nj)
    
    for i in range(ni):
        for j in range(nj):
            x = i*1./ni
            y = j*1./nj
            coords.append( (x,y)) 
            c = _idx(i,j,ni,nj)
            if i>0:
                cmi= _idx(i-1,j,ni,nj)
                conn.append((c,cmi))
            if j>0:
                cmj= _idx(i,j-1,ni,nj)
                conn.append((c,cmj))
    coords = np.array(coords)
    conn=np.array(conn)      
    return coords, conn

def try_connexion_forces():
    coords, conn = build_layout()
    return connexion_forces(coords,conn)

def test_connection_speed(benchmark):
    benchmark(try_connexion_forces)



def try_repulsion_neighbors_forces():
    coords, _ = build_layout()
    return repulsion_forces_neighbors(coords,neighbors=3)
def test_repulsion_neighbors_forces(benchmark):
    result = benchmark(try_repulsion_neighbors_forces)

def try_repulsion_macro_forces_no_level():
    coords, _ = build_layout()
    return repulsion_forces_macros(coords,max_in_quad=1)
def test_repulsion_macro_forces_no_level(benchmark):
    result = benchmark(try_repulsion_macro_forces_no_level)

def try_repulsion_macro_forces_10_per_quad():
    coords, _ = build_layout()
    return repulsion_forces_macros(coords,max_in_quad=10)
def test_repulsion_macro_forces_10_per_quad(benchmark):
    result = benchmark(try_repulsion_macro_forces_10_per_quad)

# 

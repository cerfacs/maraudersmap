""" temporary module to help Nob asserts, waiting for a full implementation in Nob"""

import numpy as np
from nob import Nob, Path

def leaves(nob):
    """Recursively build a list of all paths to leaves (no lists/dicts)"""
    paths = []
    def rec_walk(root, root_path):
        """Recursive walk of tree"""
        if isinstance(root, dict):
            for key in root:
                rec_walk(root[key], root_path / key)
        elif isinstance(root, list):
            for idx, val in enumerate(root):
                rec_walk(val, root_path / str(idx))
        else:
            paths.append(root_path)

    rec_walk(nob._data, Path())
    return paths


def smartass(actual,desired, rtol=1e-07, atol=0):
    """Select the correct assert depending on instances"""
    if isinstance(actual, float):
        assert abs(actual-desired) < atol + rtol*abs(desired)
    elif isinstance(actual, np.ndarray):
        np.testing.assert_allclose(actual, desired, atol=atol, rtol=rtol)
    else:
        assert actual == desired


def assert_nob_allclose(actual, desired, rtol=1e-07, atol=0, err_msg='', strict=True):
    """Check equality of two nested objects
    
    """
    assert_error = False
    act_nob = Nob(actual)
    des_nob = Nob(desired)
    act_paths = act_nob.leaves
    des_paths = des_nob.leaves
    out = []
    for path in des_paths:
        if path in act_paths:
            try:
                smartass(act_nob[path],des_nob[path], rtol=1e-07, atol=0)
            except AssertionError as e:
                out.append(str(path)+ " : " + str(e))
        else:
            out.append(str(path)+ " : missing in actual nested object")

    if strict:
        for path in act_paths:
            if path not in des_paths:
                out.append(str(path) + " : missing in desired nested object")
    if out:
        assert_error = True
        raise AssertionError(err_msg + "\nNob objects differ:\n"+"\n".join(out))
    return assert_error
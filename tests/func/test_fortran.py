# Test general API on Fortran datadir

from mmap_func_common import tt_score, tt_imports, tt_callgraph, tt_showgraph

def test_score_fortran(datadir):
    tt_score(datadir)

def test_imports_fortran(datadir):
    tt_imports(datadir)

def test_callgraph_fortran(datadir):
    tt_callgraph(datadir)

def test_showgraph_fortran(datadir):
    tt_showgraph(datadir)
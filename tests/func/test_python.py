# Test general API on Python datadir

from mmap_func_common import tt_score, tt_imports, tt_callgraph, tt_showgraph

def test_score_python(datadir):
    tt_score(datadir)

def test_imports_python(datadir):
    tt_imports(datadir)

def test_callgraph_python(datadir):
    tt_callgraph(datadir)

def test_showgraph_python(datadir):
    tt_showgraph(datadir)
import json
import networkx as nx
from maraudersmap.show_pyplot import fastplot

filegraph = "./out_mmap/callgraph.json"
filegraph = "./out_mmap/imports.json"

with open(filegraph,"r") as fin:
    data = json.load(fin)
    graph = nx.node_link_graph(data)
    fastplot(graph)

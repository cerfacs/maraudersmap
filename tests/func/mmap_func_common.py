"""
Func test for all languages, to be used on the different datadir

NOTE: both name of module and names of functions MUST NOT start with test_.
These are reused by test_fortran, test_python.
"""
import os
import networkx as nx
from yaml import safe_load
import json


def assert_same_graph(graph_test,graph_ref):

    if not nx.is_isomorphic(graph_test,graph_ref):
        
        print("===== Graph Mismatch ====:")
        print("Graph left:")
        print(graph_test)
        print(graph_test.nodes())
        print("Graph right:")
        print(graph_ref)
        print(graph_ref.nodes())
       
        node_mismatch =False
        for node in graph_test.nodes():
            if node not in graph_ref.nodes():
                print(f"   - node {node} is not in ref graph ")
                node_mismatch =True
        for node in graph_ref.nodes():
            if node not in graph_test.nodes():
                print(f"   - node {node} is missing ")
                node_mismatch =True
        
        if node_mismatch:
            raise AssertionError("Graphs Nodes mismatch")
        
        for edge in graph_test.edges():
            if edge not in graph_ref.edges():
                print(f"   - edge {edge} is not in ref graph ")
        for edge in graph_ref.edges():
            if node not in graph_test.edge():
                print(f"   - node {edge} is missing ")
        
        raise AssertionError("Graphs Edges mismatch")
        
                



def tt_score(datadir):
    from maraudersmap.score import get_score
    
    # ref
    with open(os.path.join(datadir, "out_mmap","func_tree_score.json"), "r") as fin:
        score_dict_ref = json.load(fin)
        score_graph_ref = nx.node_link_graph(score_dict_ref)

    # test
    with open(os.path.join(datadir, "mmap_in.yml"), "r") as fin:
        param = safe_load(fin)

    # test
    with open(os.path.join(datadir, "out_mmap","func_tree.json"), "r") as fin:
        tree_dict = json.load(fin)
        tree_graph = nx.node_link_graph(tree_dict)

    # with open(os.path.join(datadir,"fortran_rc_default.yml"), "r") as fin:
    #     rules = safe_load(fin)
   

    rules = os.path.join(datadir,"test_rules.yml")
    score_graph = get_score(
        os.path.join(datadir,param["path"]),
        tree_graph,
        rules)
    score_dict =  nx.node_link_data(score_graph)
    
    # check json
    #assert score_dict == score_dict_ref
    # check graph
    assert_same_graph(score_graph,score_graph_ref)
    #assert nx.is_isomorphic(score_graph,score_graph_ref)


def tt_imports(datadir):
    from maraudersmap.imports import get_importsgraph
    
    with open(os.path.join(datadir, "mmap_in.yml"), "r") as fin:
        param = safe_load(fin)
    print(param["path"])
    import_graph = get_importsgraph(
        os.path.join(datadir,param["path"]),
    )
    import_dict = nx.node_link_data(import_graph)

    with open(os.path.join(datadir, "out_mmap","importsgraph.json"), "r") as fin:
        import_dict_ref = json.load(fin)
        import_graph_ref= nx.node_link_graph(import_dict_ref)

    # check json

    print(import_dict)
    print("New ^^^^, Target vvvvv")
    print(import_dict_ref)
    
    #assert assert_nob_allclose(import_dict, import_dict_ref)
    # check graph
    assert_same_graph(import_graph,import_graph_ref)
    
    #assert nx.is_isomorphic(import_graph,import_graph_ref)



def tt_callgraph(datadir):

    from maraudersmap.callgraph import get_callgraph

    with open(os.path.join(datadir, "out_mmap","callgraph.json"), "r") as fin:
        callgraph_dict_ref = json.load(fin)
        callgraph_graph_ref = nx.node_link_graph(callgraph_dict_ref)

    path = os.path.join(datadir,"src")

    callgraph_graph = get_callgraph(path, path)#, wkdir=datadir)
    
    # check json
    # callgraph_dict = nx.node_link_data(callgraph_graph)
    #assert callgraph_dict == callgraph_dict_ref
    # check graph
    assert_same_graph(callgraph_graph,callgraph_graph_ref)
    #assert nx.is_isomorphic(callgraph_graph,callgraph_graph_ref)


def tt_showgraph(datadir):
    from maraudersmap.show_pyvis import showgraph_pyvis
    from maraudersmap.full_graph_actions import (
        clean_graph,
        #color_nodes_by_quantity,
        color_nodes_by_pattern,
    )    
    #from  maraudersmap.cli import ensure_dir


    file = os.path.join(datadir, "mmap_in.yml")
    with open(file, "r") as fin:
        param = safe_load(fin)

    with open(os.path.join(datadir, "out_mmap","callgraph.json"), "r") as fin:
        callgraph_dict = json.load(fin)

    cgs_nx = nx.node_link_graph(callgraph_dict)

    cgs_nx, legend = color_nodes_by_pattern(cgs_nx, param["color_rules"])
   
    cgs_nx = clean_graph(
        cgs_nx,
        remove_patterns=param["clean_graph"].get("remove_patterns",[]),
        hyperconnect=param["clean_graph"].get("remove_hyperconnect",5),
        subgraph_roots=param["clean_graph"].get("subgraph_roots", []),
    )
    graphs =  "callgraph"

    #outdir = ensure_dir(param["package"])
    file_prefix = f"{param['package']}_{graphs}"

    showgraph_pyvis(cgs_nx, legend, file_prefix, writeondisc=False)
import json
import networkx as nx
from maraudersmap.show_pyplot import fastplot


with open("./testftn/callgraph.json") as fin:
    data = json.load(fin)
    graph = nx.node_link_graph(data)
    fastplot(graph)
program estimate_pi
  use monte_carlo_module_dp, only : generate_points_dp,is_inside_unit_circle_dp
  use counts_hits_module, only: count_hits
  use monte_carlo_module_h
  
  implicit none

  integer :: n_points, n_hits
  real :: pi_estimate

  ! Set the number of points to use in the Monte Carlo simulation
  n_points = 1000000

  ! Generate the points and count the number of hits
  call generate_points(n_points, n_hits)

  ! Estimate the value of pi
  pi_estimate = 4 * count_hits(n_hits, n_points)

  ! Print the estimate
  print *, 'Estimate of pi (sp):', pi_estimate

  ! DOUBLE PRECISION ========

  ! Generate the points and count the number of hits
  call generate_points_dp(n_points, n_hits)

  ! Estimate the value of pi
  pi_estimate = 4 * count_hits(n_hits, n_points)

  ! Print the estimate
  print *, 'Estimate of pi (dp):', pi_estimate


end program estimate_pi


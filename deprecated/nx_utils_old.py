

def get_fun_size(funstr: str) -> int:
    """
    Return the size of functions in lines

    Args:
        funstr (str): Name of the function

    Returns:
        lenght of the function
    """

    print(">>>???", funstr)
    spl = funstr.split(".")
    # if len(spl)<3:
    #     return 50

    # modstr = ".".join(spl[:-1])

    module_imptd = False

    print(f"DEBUG : Evaluating {funstr}")

    if len(spl) <= 1:
        print(f"DEBUG : {funstr} is not a module+attribute name, skipping")
        return 50

    idx = 1
    for recall in spl:
        modstr = ".".join(spl[:idx])
        print(f"DEBUG : Get mod:", modstr)

        try:
            # module= importlib.import_module(spl[0]+"."+spl[1])
            module = importlib.import_module(modstr)
            module_imptd = True
            goodmodstr = modstr
            idx += 1
            continue
        except ModuleNotFoundError:
            pass

    if not module_imptd:
        print(f"DEBUG : ModuleNotFoundError  while getting module {modstr}")
        return 50

    print(f"DEBUG : Found module {goodmodstr}")
    # Quick exit if module is enough
    if goodmodstr == funstr:
        try:
            lines = inspect.getsource(module)
            return len(lines.split("\n"))
        except OSError:
            print(f"OSError while getting module {goodmodstr}")
            return 10

        except TypeError:
            print(f"TypeError while getting module {goodmodstr}")
            return 10

    attr_imptd = False
    idx -= 1
    attrstr = ".".join(spl[: idx + 1])
    print(f"DEBUG :Get attr", attrstr)
    try:
        call = getattr(module, spl[idx])
    except AttributeError:
        print(f"OSError while getting attribute {attrstr}")
        return 10
    lastgoodstr = attrstr
    attr_imptd = True

    while idx < len(spl):
        idx += 1
        attrstr = ".".join(spl[: idx + 1])
        print(f"DEBUG :Get attr", attrstr)
        try:
            call = getattr(call, recall)
            attr_imptd = True
            lastgoodstr = attrstr
        except AttributeError:
            break

    if not attr_imptd:
        print(f"AttributeNotFoundError  while getting attribute {lastgoodstr}")

    # #     return 50
    # # except TypeError:
    # #     print(f"TypeError while getting module {modstr}")
    # #     return 50

    # call = getattr(module,spl[2])
    # for recall in spl[3:]:
    #     try:
    #         call = getattr(call,recall)
    #     except AttributeError:
    #         print(f"AttributeError while getting attribute {call}.{recall}")

    #         return 10

    try:
        lines = inspect.getsource(call)
    except TypeError:
        print(f"TypeError while getting lines {call}")
        return 10
    except OSError:
        print(f"OSError while getting lines {call}")
        return 10
    return len(lines.split("\n"))


def ensure_parentality(ntx: nx.DiGraph) -> nx.DiGraph:
    """
    Create nodes to include all ancestors of each node
    Usefull to bind object methodes togethers

    Args:
        ntx (obj): ntx (obj): networkX DiGraph

    Returns:
        ntx (obj): networkX DiGraph with all ancestors included
    """
    nt_new = deepcopy(ntx)

    for node in nt:
        parent = ".".join(node.split(".")[:-1])
        if parent not in nt_new:
            if len(parent.split(".")) > 1:
                nt_new.add_node(parent, size=get_fun_size(parent), soften=True)

        if parent in nt_new:
            nt_new.add_edge(parent, node, soften=True)
            # print(f"adding {parent} -> {node}")
    return nt_new

Adding Lizard sources for experiments to stay compatible with python 3.11
Revert back to lizard official as soon as changes accepted

changes on :
    lizard_languages/code_analyzer.py (case_sensitive option)
    lizard_languages/fortran.py (case_sensitive option)

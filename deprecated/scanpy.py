
USUAL_TYPES= ["str","float", "int", "list", "dict"]
USUAL_FUNCTIONS= ["open","print", "enumerate"]
USUAL_METHODS= [".append",".extend"]

def read_file(filename):
    with open(filename, "r") as fin:
        data= fin.readlines()

    batch=""
    list_lines=[]
    level=0
    for line in data:
        for char in line:
            if char in "([{":
                level+=1
            if char in ")]}":
                level-=1
        batch += line.strip("\n")
        if level == 0:
            list_lines.append(batch+"\n")
            batch=""
    return list_lines



def find_funcs(list_lines):
    cur_func = None
    calls=[]
    imports={}
    args={}
    indocstring =False
    for line in list_lines:
        #print(line, indocstring)
        if line.strip().startswith('"""'):
            indocstring=True
        if line.strip().endswith('"""'):
            indocstring=False
        
        if not indocstring:
            if cur_func is None:
                imports.update(find_imports(line))
            else:
                calls.extend(find_call(line))
            
            if line.startswith("def "):
                # flush prev func
                print(cur_func)

                calls = filter_calls(calls, imports, args)
                for call in calls:
                    print(f"   |{call}|")
                print("="*10)
                

                name, args = parse_def(line)
                cur_func= name
                calls=[]
    

def find_imports(line:str)-> dict:
    imports= {}
    if "import " not in line:
        return []
    if line.startswith("import "):
        if " as " in line:
            imps,reproxys = line[6:].split(" as ")
            for i,imp in enumerate(imps.split(",")):
                imports[reproxys.split(",")[i].strip()] =imp.strip()
        else:
            for imp in line[6:].split(","):
                imports[imp.strip()]=  imp.strip()

    if line.startswith("from "):
        ref, imps = line[5:].split(" import ")
        for imp in imps.split(","):
            imports[imp.strip()] = ref+"."+imp.strip()
    

    return imports
        

def filter_calls(calls:list, imports:dict, args:dict)->list:

    fcalls = []
    for call in calls:
        interesting = True
        if call in USUAL_TYPES+ USUAL_FUNCTIONS:
            interesting=False
        for meth in USUAL_METHODS:
            if call.endswith(meth):
                interesting=False
        if interesting:
            if call in args and args[call] is not None:
                call = args[call]

            if call in imports:
                call = imports[call]
            fcalls.append(call)
    
    return list(set(fcalls))

def find_call(line:str) -> list:

    calls = []
    batch = ""

    if line.startswith("def "):
        return []
    for char in line:
        batch+=char
        if char in " +-=":
            batch = ""
        if char == "(":
            candidate=batch[:-1].strip()
            if not candidate:
                continue
            calls.append(candidate)
    return calls




def parse_def(line:str):
    pivot = line.index("(")

    name = line[3:pivot]
    allargs = line[pivot+1:-3].strip(" ")

    args={}
    for defarg in allargs.split(','):
        arg = defarg
        proxyfor = None
        kw=False
        if "=" in defarg:
            arg = defarg.split("=")[0]
        if ":" in arg:
            arg, proxyfor = arg.split(":")
            if proxyfor in USUAL_TYPES:
                proxyfor = None     
        
        args[arg]={"kw":kw, "proxyfor":proxyfor}
    
    return name, args
        


def parse_args(line:str) -> dict:
    pivot = line.index("(")

    name = line[3:pivot]
    allargs = line[pivot+1:-3].strip(" ")

    args={}
    for defarg in allargs.split(','):
        arg = defarg
        proxyfor = None
        kw=False
        if "=" in defarg:
            arg = defarg.split("=")[0]
        if ":" in arg:
            arg, proxyfor = arg.split(":")
            if proxyfor in USUAL_TYPES:
                proxyfor = None     
        
        args[arg]={"kw":kw, "proxyfor":proxyfor}
    
    return args
        



llines = read_file("/Users/dauptain/GITLAB/pyavbp/src/pyavbp/gui/combu/process_finish.py")

find_funcs(llines)

with open("tmp.py", "w") as fout:
    fout.writelines(llines)
"""Sample code to illustrate script identification"""
import numpy as np

x = 12
y = 3
mean_value = np.mean([x, y])
sum_value = x + y
diff_value = x - y
array = np.array([x, y])

"""
Func test for imports.py file for fortran code
"""

import pytest
import os

from pathlib import Path
from maraudersmap.imports import (
    compute_importsgraph_db,
    _rec_analyze_files,
    build_importsgraph,
    get_importsgraph,
)
from maraudersmap.macro_tree import get_macro_tree


def test_compute_importsgraph_db(datadir):
    """Testing the database computation for 1 simple file"""
    code_name = "test_imports_fortran"
    fimports = {}
    filename = os.path.join(datadir, "estimate_pi.f90")
    fimports = compute_importsgraph_db(filename, fimports, code_name)

    assert len(fimports["estimate_pi.f90"]) == 3
    assert sorted(list(fimports["estimate_pi.f90"].keys())) == [
        "counts_hits_module",
        "monte_carlo_module_dp",
        "monte_carlo_module_h",
    ]
    assert fimports["estimate_pi.f90"]["monte_carlo_module_dp"] == [
        "generate_points_dp",
        "is_inside_unit_circle_dp",
    ]
    assert fimports["estimate_pi.f90"]["counts_hits_module"] == ["count_hits"]
    assert fimports["estimate_pi.f90"]["monte_carlo_module_h"] == []


def test_rec_analyze_files_file(datadir):
    """Testing the recursive build of imports database starting from a header file"""
    code_name = "test_imports_fortran"
    fimports = {}
    repo_path = os.path.join(datadir, "monte_carlo_module_h.f90")
    fimports = _rec_analyze_files(Path(repo_path), fimports, code_name)

    assert len(fimports) == 1
    assert ["monte_carlo_module_h.f90"] == list(fimports.keys())
    assert list(fimports["monte_carlo_module_h.f90"].keys()) == ["monte_carlo_module"]
    assert fimports["monte_carlo_module_h.f90"]["monte_carlo_module"] == [
        "generate_points",
        "is_inside_unit_circle",
    ]


def test_rec_analyze_files_folder(datadir):
    """Testing the recursive build of imports database for a folder"""
    code_name = "test_imports_fortran"
    fimports = {}
    fimports = _rec_analyze_files(Path(datadir), fimports, code_name)

    expected_files = [
        "counts_hits_module.f90",
        "estimate_pi.f90",
        "monte_carlo_module.f90",
        "monte_carlo_module_dp.f90",
        "monte_carlo_module_h.f90",
    ]
    assert len(fimports) == 5
    for file in expected_files:
        assert file in list(fimports.keys())

    assert fimports["counts_hits_module.f90"] == {}

    assert sorted(list(fimports["estimate_pi.f90"].keys())) == [
        "counts_hits_module",
        "monte_carlo_module_dp",
        "monte_carlo_module_h",
    ]
    assert fimports["estimate_pi.f90"]["counts_hits_module"] == ["count_hits"]
    assert fimports["estimate_pi.f90"]["monte_carlo_module_dp"] == [
        "generate_points_dp",
        "is_inside_unit_circle_dp",
    ]
    assert fimports["estimate_pi.f90"]["monte_carlo_module_h"] == []

    assert fimports["monte_carlo_module.f90"] == {}
    assert fimports["monte_carlo_module_dp.f90"] == {}

    assert list(fimports["monte_carlo_module_h.f90"].keys()) == ["monte_carlo_module"]
    assert fimports["monte_carlo_module_h.f90"]["monte_carlo_module"] == [
        "generate_points",
        "is_inside_unit_circle",
    ]


def test_build_importsgraph(datadir):
    """
    Checking the creation of the graph thus comparing names of nodes as well as the edges created
    """
    code_name = "test_imports_fortran"
    macro_graph = get_macro_tree(datadir, code_name)

    fimports = {}
    fimports = _rec_analyze_files(Path(datadir), fimports, code_name)
    imports_graph = build_importsgraph(fimports, macro_graph, code_name)
    expected_nodes = [
        "test_imports_fortran/counts_hits_module.f90",
        "test_imports_fortran/estimate_pi.f90",
        "test_imports_fortran/monte_carlo_module.f90",
        "test_imports_fortran/monte_carlo_module_dp.f90",
        "test_imports_fortran/monte_carlo_module_h.f90",
    ]
    expected_edges = [
        (
            "test_imports_fortran/estimate_pi.f90",
            "test_imports_fortran/counts_hits_module.f90",
        ),
        (
            "test_imports_fortran/estimate_pi.f90",
            "test_imports_fortran/monte_carlo_module_dp.f90",
        ),
        (
            "test_imports_fortran/estimate_pi.f90",
            "test_imports_fortran/monte_carlo_module_h.f90",
        ),
        (
            "test_imports_fortran/monte_carlo_module_h.f90",
            "test_imports_fortran/monte_carlo_module.f90",
        ),
    ]

    assert sorted(list(imports_graph.nodes())) == expected_nodes
    assert sorted(list(imports_graph.edges())) == expected_edges


def test_get_importsgraph(datadir):
    """
    Just checking the whole creation of graph, comparing the number of nodes and edges expected
    """
    code_name = "test_imports_fortran"
    imports_graph = get_importsgraph(datadir, code_name)
    assert len(imports_graph.nodes()) == 5
    assert len(imports_graph.edges()) == 4

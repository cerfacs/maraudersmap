"""
Func test for callgraph.py file for fortran code
"""

import pytest
import os

import networkx as nx
from pathlib import Path
from maraudersmap.tree import get_tree
from maraudersmap.imports import get_importsgraph
from maraudersmap.callgraph import get_calls, analyze_files, get_callgraph


def test_get_calls1():
    """Testing the parser for routine content with fortran script"""
    content = [
        "program estimate_pi\n",
        "  use monte_carlo_module_dp, only : generate_points_dp,is_inside_unit_circle_dp\n",
        "  use counts_hits_module, only: count_hits\n",
        "  use monte_carlo_module_h\n",
        "  \n",
        "  implicit none\n",
        "\n",
        "  integer :: n_points, n_hits\n",
        "  real :: pi_estimate\n",
        "\n",
        "  ! Set the number of points to use in the Monte Carlo simulation\n",
        "  n_points = 1000000\n",
        "\n",
        "  ! Generate the points and count the number of hits\n",
        "  call generate_points(n_points, n_hits)\n",
        "\n",
        "  ! Estimate the value of pi\n",
        "  pi_estimate = 4 * count_hits(n_hits, n_points)\n",
        "\n",
        "  ! Print the estimate\n",
        "  print *, 'Estimate of pi (sp):', pi_estimate\n",
        "\n",
        "  ! DOUBLE PRECISION ========\n",
        "\n",
        "  ! Generate the points and count the number of hits\n",
        "  call generate_points_dp(n_points, n_hits)\n",
        "\n",
        "  ! Estimate the value of pi\n",
        "  pi_estimate = 4 * count_hits(n_hits, n_points)\n",
        "\n",
        "  ! Print the estimate\n",
        "  print *, 'Estimate of pi (dp):', pi_estimate\n",
        "\n",
        "\n",
        "end program estimate_pi\n",
        "\n",
    ]
    func_calls = get_calls(content)
    expected_func = ["count_hits", "generate_points", "generate_points_dp", "pi"]
    assert sorted(func_calls) == sorted(expected_func)


def test_get_calls2():
    """Testing the parser for routine content with fortran subroutine"""
    content = [
        "  subroutine generate_points(n_points, n_hits)\n",
        "    integer, intent(in) :: n_points\n",
        "    integer, intent(out) :: n_hits\n",
        "    real :: x, y\n",
        "    integer  :: i\n",
        "\n",
        "    ! Initialize random number generator\n",
        "    call random_seed()\n",
        "\n",
        "    ! Generate random points and count the number of hits\n",
        "    n_hits = 0\n",
        "    do i = 1, n_points\n",
        "      call random_number(x)\n",
        "      call random_number(y)\n",
        "      if (is_inside_unit_circle(x, y)) then\n",
        "        n_hits = n_hits + 1\n",
        "      end if\n",
        "    end do\n",
        "\n",
        "  end subroutine generate_points\n",
        "\n",
    ]
    func_calls = get_calls(content)
    expected_func = [
        "is_inside_unit_circle",
        "intent",
        "random_seed",
        "random_number",
        "if",
    ]
    assert sorted(func_calls) == sorted(expected_func)


def test_analyze_files(datadir):
    """Testing creation of called func database"""
    code_name = "test_callgraph_fortran"
    file = os.path.join(datadir, "estimate_pi.f90")
    tree_graph = get_tree(file, code_name)

    node = tree_graph.nodes["estimate_pi.f90:__script__"]
    func_calls = {}
    func_calls = analyze_files(node, func_calls)
    assert sorted(func_calls["estimate_pi.f90:__script__"]) == sorted(
        [
            "generate_points",
            "generate_points_dp",
            "count_hits",
            "pi",
        ]
    )


def test_get_callgraph1(datadir):
    """Testing the general creation of callgraph for a repo"""
    code_name = "test_callgraph_fortran"
    imports_graph = get_importsgraph(datadir, code_name)
    tree_graph = get_tree(datadir, code_name)

    callgraph = get_callgraph(tree_graph, imports_graph)
    expected_nodes = [
        "test_callgraph_fortran/counts_hits_module.f90:counts_hits_module::count_hits",
        "test_callgraph_fortran/estimate_pi.f90:__script__",
        "test_callgraph_fortran/monte_carlo_module.f90:monte_carlo_module::generate_points",
        "test_callgraph_fortran/monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
        "test_callgraph_fortran/monte_carlo_module_dp.f90:monte_carlo_module_dp::generate_points_dp",
        "test_callgraph_fortran/monte_carlo_module_dp.f90:monte_carlo_module_dp::is_inside_unit_circle_dp",
        "test_callgraph_fortran/monte_carlo_module_h.f90:monte_carlo_module_h::dummy_monte_carlo_module_h",
    ]
    expected_edges = [
        (
            "test_callgraph_fortran/estimate_pi.f90:__script__",
            "test_callgraph_fortran/counts_hits_module.f90:counts_hits_module::count_hits",
        ),
        (
            "test_callgraph_fortran/estimate_pi.f90:__script__",
            "test_callgraph_fortran/monte_carlo_module.f90:monte_carlo_module::generate_points",
        ),
        (
            "test_callgraph_fortran/estimate_pi.f90:__script__",
            "test_callgraph_fortran/monte_carlo_module_dp.f90:monte_carlo_module_dp::generate_points_dp",
        ),
        (
            "test_callgraph_fortran/monte_carlo_module.f90:monte_carlo_module::generate_points",
            "test_callgraph_fortran/monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
        ),
        (
            "test_callgraph_fortran/monte_carlo_module_dp.f90:monte_carlo_module_dp::generate_points_dp",
            "test_callgraph_fortran/monte_carlo_module_dp.f90:monte_carlo_module_dp::is_inside_unit_circle_dp",
        ),
    ]
    assert sorted(list(callgraph.nodes())) == expected_nodes
    assert sorted(list(callgraph.edges())) == expected_edges


def test_get_callgraph2(datadir):
    """Testing the general creation of callgraph for a single file"""
    code_name = "test_callgraph_fortran"
    repo_path = os.path.join(datadir, "monte_carlo_module.f90")
    imports_graph = get_importsgraph(repo_path, code_name)
    tree_graph = get_tree(repo_path, code_name)

    callgraph = get_callgraph(tree_graph, imports_graph)
    expected_nodes = [
        "monte_carlo_module.f90:monte_carlo_module::generate_points",
        "monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
    ]
    expected_edges = [
        (
            "monte_carlo_module.f90:monte_carlo_module::generate_points",
            "monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
        )
    ]
    assert sorted(list(callgraph.nodes())) == expected_nodes
    assert sorted(list(callgraph.edges())) == expected_edges


def test_get_callgraph3_killswitch_off(datadir):
    """Testing the creation of callgraph without the killswitch on"""
    code_name = "test_callgraph_fortran"
    repo_path = os.path.join(datadir, "monte_carlo_module.f90")
    imports_graph = get_importsgraph(repo_path, code_name)
    tree_graph = get_tree(repo_path, code_name)

    callgraph = get_callgraph(tree_graph, imports_graph, killswitch=False)
    expected_nodes = [
        "if",
        "intent",
        "monte_carlo_module.f90:monte_carlo_module::generate_points",
        "monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
        "random_number",
        "random_seed",
    ]
    expected_edges = [
        ("monte_carlo_module.f90:monte_carlo_module::generate_points", "if"),
        ("monte_carlo_module.f90:monte_carlo_module::generate_points", "intent"),
        (
            "monte_carlo_module.f90:monte_carlo_module::generate_points",
            "monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
        ),
        ("monte_carlo_module.f90:monte_carlo_module::generate_points", "random_number"),
        ("monte_carlo_module.f90:monte_carlo_module::generate_points", "random_seed"),
        ("monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle", "intent"),
    ]
    assert sorted(list(callgraph.nodes())) == expected_nodes
    assert sorted(list(callgraph.edges())) == expected_edges

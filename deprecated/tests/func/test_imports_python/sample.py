"""Sample code to illustrate the usage of dynamic callgraph generation..."""
# Imports are reduce to the current folder for tests purposes it thus emulates the folder as a repo
# This is mandatory in order to run the tests properly
import test_imports_python.script


def fuuun(x, y, opt=False):
    out = addition(x, y)

    if out > 10:
        out = toobig(out)

    if opt:
        maybemaybenot()
    return out


def addition(x, y):
    return y + x


def toobig(x):
    return x - 10


def maybemaybenot():
    pass
    return

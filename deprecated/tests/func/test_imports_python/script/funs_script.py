"""Sample code to illustrate script identification"""
import numpy as np

# Imports are reduce to the current folder for tests purposes it thus emulates the folder as a repo
# This is mandatory in order to run the tests properly
from test_imports_python.objs import myObject as custom_obj
from test_imports_python.funs import (
    fun_foo,
    fun_ext,
    fun_void,
    fun_bar,
    main,
    fun_with_obj,
)

x = 12
y = 3
mean_value = np.mean([x, y])
sum_value = x + y
diff_value = x - y
array = np.array([x, y])

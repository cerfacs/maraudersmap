"""
Func test for macro_tree.py file for python code
"""

import os
from maraudersmap.tree import get_tree
from maraudersmap.macro_tree import (
    _rec_to_macro_tree,
    _rec_node_to_erase,
    get_macro_tree,
)


def test_rec_node_to_erase(datadir):
    """Test to find the list of nodes to be erased"""
    path = os.path.join(datadir, "funs.py")
    code_name = "test_macro_tree_python"
    graph = get_tree(path, code_name)
    expected_erase = [
        "funs.py:fun_foo",
        "funs.py:fun_ext",
        "funs.py:fun_void",
        "funs.py:fun_bar",
        "funs.py:main",
        "funs.py:fun_with_obj",
    ]
    assert _rec_node_to_erase(graph, "funs.py") == expected_erase


def test_rec_to_macro_tree_file(datadir):
    """Test to switch leaves to parent node"""
    path = os.path.join(datadir, "funs.py")
    code_name = "test_tree_python"
    graph = get_tree(path, code_name)

    for node in _rec_node_to_erase(graph, "funs.py"):
        graph.remove_node(node)
    _rec_to_macro_tree(graph, "funs.py")
    assert len(graph.nodes()) == 1
    assert graph.nodes["funs.py"]["leaf"] == True


def test_rec_to_macro_tree_folder(datadir):
    """Test to switch leaves to parent node"""
    code_name = "test_macro_tree_python"
    graph = get_tree(datadir, code_name)

    for node in _rec_node_to_erase(graph, "test_macro_tree_python"):
        graph.remove_node(node)

    _rec_to_macro_tree(graph, "test_macro_tree_python")

    assert graph.nodes["test_macro_tree_python/objs.py"]["leaf"] == True
    assert graph.nodes["test_macro_tree_python/funs.py"]["leaf"] == True
    assert graph.nodes["test_macro_tree_python/sample.py"]["leaf"] == True
    assert graph.nodes["test_macro_tree_python/funs_script.py"]["leaf"] == True


def test_get_macro_tree(datadir):
    """Test whole routine, making sure we have the right number of nodes"""
    code_name = "test_macro_tree_python"
    graph = get_macro_tree(datadir, code_name)
    assert len(graph) == 5

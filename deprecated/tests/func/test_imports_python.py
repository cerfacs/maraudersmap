"""
Func test for imports.py file for python code
"""

import pytest
import os

from pathlib import Path
from maraudersmap.imports import (
    compute_importsgraph_db,
    _rec_analyze_files,
    build_importsgraph,
    get_importsgraph,
)
from maraudersmap.macro_tree import get_macro_tree


def test_compute_importsgraph_db(datadir):
    """Testing the generalisation, looking at the size of outputs."""
    code_name = "script"
    fimports = {}
    filename = os.path.join(datadir, "script/funs_script.py")
    fimports = compute_importsgraph_db(filename, fimports, code_name)

    assert len(fimports["funs_script.py"]) == 3
    assert list(fimports["funs_script.py"].keys()) == [
        "numpy",
        "test_imports_python.objs",
        "test_imports_python.funs",
    ]
    assert fimports["funs_script.py"]["numpy"] == ["np"]
    assert fimports["funs_script.py"]["test_imports_python.objs"] == ["myObject"]
    assert fimports["funs_script.py"]["test_imports_python.funs"] == [
        "fun_foo",
        "fun_ext",
        "fun_void",
        "fun_bar",
        "main",
        "fun_with_obj",
    ]


def test_rec_analyze_files_file(datadir):
    """Testing the recursive build of imports database starting from a file"""
    code_name = "test_imports_python"
    fimports = {}
    repo_path = os.path.join(datadir, "funs.py")
    fimports = _rec_analyze_files(Path(repo_path), fimports, code_name)

    assert len(fimports) == 1
    assert ["funs.py"] == list(fimports.keys())
    assert list(fimports["funs.py"].keys()) == ["numpy", "test_imports_python.objs"]
    assert fimports["funs.py"]["test_imports_python.objs"] == [
        "myObject",
        "external_fun",
    ]
    assert fimports["funs.py"]["numpy"] == ["np"]


def test_rec_analyze_files_folder(datadir):
    """Testing the recursive build of imports database for a folder"""
    code_name = "test_imports_python"
    fimports = {}
    fimports = _rec_analyze_files(Path(datadir), fimports, code_name)

    expected_files = [
        "dummy.yml",
        "funs.py",
        "objs.py",
        "sample.py",
        "script/funs_script.py",
    ]
    assert len(fimports) == 5
    for file in expected_files:
        assert file in list(fimports.keys())
    assert list(fimports["funs.py"].keys()) == ["numpy", "test_imports_python.objs"]
    assert fimports["funs.py"]["test_imports_python.objs"] == [
        "myObject",
        "external_fun",
    ]
    assert fimports["funs.py"]["numpy"] == ["np"]
    assert fimports["objs.py"] == {}
    assert list(fimports["script/funs_script.py"].keys()) == [
        "numpy",
        "test_imports_python.objs",
        "test_imports_python.funs",
    ]
    assert fimports["script/funs_script.py"]["numpy"] == ["np"]
    assert fimports["script/funs_script.py"]["test_imports_python.objs"] == ["myObject"]
    assert fimports["script/funs_script.py"]["test_imports_python.funs"] == [
        "fun_foo",
        "fun_ext",
        "fun_void",
        "fun_bar",
        "main",
        "fun_with_obj",
    ]
    assert list(fimports["sample.py"].keys()) == ["test_imports_python.script"]
    assert fimports["sample.py"]["test_imports_python.script"] == []


def test_build_importsgraph(datadir):
    """
    Checking the creation of the graph thus comparing names of nodes as well as the edges created
    """
    code_name = "test_imports_python"
    macro_graph = get_macro_tree(datadir, code_name)

    fimports = {}
    fimports = _rec_analyze_files(Path(datadir), fimports, code_name)
    imports_graph = build_importsgraph(fimports, macro_graph, code_name)
    expected_nodes = [
        "numpy",
        "test_imports_python/funs.py",
        "test_imports_python/objs.py",
        "test_imports_python/sample.py",
        "test_imports_python/script",
        "test_imports_python/script/funs_script.py",
    ]
    expected_edges = [
        ("test_imports_python/funs.py", "numpy"),
        ("test_imports_python/funs.py", "test_imports_python/objs.py"),
        ("test_imports_python/sample.py", "test_imports_python/script"),
        ("test_imports_python/script/funs_script.py", "numpy"),
        ("test_imports_python/script/funs_script.py", "test_imports_python/funs.py"),
        ("test_imports_python/script/funs_script.py", "test_imports_python/objs.py"),
    ]

    assert sorted(list(imports_graph.nodes())) == expected_nodes
    assert sorted(list(imports_graph.edges())) == expected_edges


def test_get_importsgraph(datadir):
    """
    Just checking the whole creation of graph, comparing the number of nodes and edges expected
    """
    code_name = "test_imports_python"
    imports_graph = get_importsgraph(datadir, code_name)
    assert len(imports_graph.nodes()) == 6
    assert len(imports_graph.edges()) == 6

"""
Unit test for tree.py file for python code
"""

import pytest
import os

import networkx as nx
from pathlib import Path
from maraudersmap.tree import (
    get_tree_nx_file,
    _rec_get_tree_nx_folder,
    _rec_node_stats,
    get_tree,
)


def test_get_tree_nx_file(datadir):
    """Testing if we have the right nodes and edges, sorted to avoid names wrong positionning"""
    file = os.path.join(datadir, "funs.py")
    node_key = "funs.py"
    graph = nx.DiGraph()
    graph = get_tree_nx_file(file, node_key, graph)
    edges = sorted(list(graph.edges))
    expected_edges = [
        ("funs.py", "funs.py:fun_bar"),
        ("funs.py", "funs.py:fun_ext"),
        ("funs.py", "funs.py:fun_foo"),
        ("funs.py", "funs.py:fun_void"),
        ("funs.py", "funs.py:fun_with_obj"),
        ("funs.py", "funs.py:main"),
    ]

    assert sorted(list(graph.nodes)) == [
        "funs.py",
        "funs.py:fun_bar",
        "funs.py:fun_ext",
        "funs.py:fun_foo",
        "funs.py:fun_void",
        "funs.py:fun_with_obj",
        "funs.py:main",
    ]
    assert edges == expected_edges


def test_rec_get_tree_nx_folder(datadir):
    """Test of identification of nodes, making sure the amount of nodes is right"""
    repo_path = Path(datadir)
    current_path = Path(datadir)
    code_name = "func"
    graph = nx.DiGraph()
    graph = _rec_get_tree_nx_folder(repo_path, current_path, code_name, graph)

    assert len(graph.nodes) == 19
    assert sorted(list(graph.nodes())) == [
        "test_tree_python/funs.py",
        "test_tree_python/funs.py:fun_bar",
        "test_tree_python/funs.py:fun_ext",
        "test_tree_python/funs.py:fun_foo",
        "test_tree_python/funs.py:fun_void",
        "test_tree_python/funs.py:fun_with_obj",
        "test_tree_python/funs.py:main",
        "test_tree_python/funs_script.py",
        "test_tree_python/funs_script.py:__script__",
        "test_tree_python/objs.py",
        "test_tree_python/objs.py:__init__",
        "test_tree_python/objs.py:external_fun",
        "test_tree_python/objs.py:meth1",
        "test_tree_python/objs.py:meth2",
        "test_tree_python/sample.py",
        "test_tree_python/sample.py:addition",
        "test_tree_python/sample.py:fuuun",
        "test_tree_python/sample.py:maybemaybenot",
        "test_tree_python/sample.py:toobig",
    ]


def test_rec_node_stats(datadir):
    """Test udpate of nodes parameters"""
    file = os.path.join(datadir, "funs.py")
    node_key = "test_tree_python/funs.py"
    graph = nx.DiGraph()
    graph = get_tree_nx_file(file, node_key, graph)
    _rec_node_stats(graph, node_key)
    expected_modules = [
        "fun_bar",
        "fun_ext",
        "fun_foo",
        "fun_void",
        "fun_with_obj",
        "main",
    ]
    assert len(graph.nodes["test_tree_python/funs.py"]["module"]) == len(
        expected_modules
    )
    for module in expected_modules:
        assert module in graph.nodes["test_tree_python/funs.py"]["module"]
    assert graph.nodes["test_tree_python/funs.py"]["size"] == 21
    assert graph.nodes["test_tree_python/funs.py"]["ccn"] == 1
    assert graph.nodes["test_tree_python/funs.py"]["leaf"] == False
    assert graph.nodes["test_tree_python/funs.py"]["soften"] == True

    assert graph.nodes["test_tree_python/funs.py:main"]["leaf"] == True
    assert graph.nodes["test_tree_python/funs.py:fun_bar"]["leaf"] == True
    assert graph.nodes["test_tree_python/funs.py:fun_void"]["leaf"] == True
    assert graph.nodes["test_tree_python/funs.py:fun_foo"]["leaf"] == True
    assert graph.nodes["test_tree_python/funs.py:fun_with_obj"]["leaf"] == True
    assert graph.nodes["test_tree_python/funs.py:fun_ext"]["leaf"] == True


def test_get_tree_file(datadir):
    """Test of the whole routine for a single file, making sure we ahve the right number of nodes"""
    path = os.path.join(datadir, "funs.py")
    code_name = "test_tree_python"
    graph = get_tree(path, code_name)
    expected_nodes = [
        "funs.py",
        "funs.py:fun_bar",
        "funs.py:fun_ext",
        "funs.py:fun_foo",
        "funs.py:fun_void",
        "funs.py:fun_with_obj",
        "funs.py:main",
    ]
    assert sorted(list(graph.nodes())) == expected_nodes


def test_get_tree_folder(datadir):
    """Test of the whole routine, making sure with have the right number of nodes"""
    code_name = "test_tree_python"
    graph = get_tree(datadir, code_name)
    assert len(graph.nodes()) == 20

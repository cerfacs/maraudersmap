module monte_carlo_module_h

  use monte_carlo_module, only :  generate_points,&
    is_inside_unit_circle

    implicit none

    contains

      subroutine dummy_monte_carlo_module_h
        implicit none
      end subroutine

end module monte_carlo_module_h

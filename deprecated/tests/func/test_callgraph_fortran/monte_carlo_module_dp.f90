module monte_carlo_module_dp
  implicit none

contains

  subroutine generate_points_dp(n_points, n_hits)
    integer, intent(in) :: n_points
    integer, intent(out) :: n_hits
    double precision:: x, y
    integer  :: i

    ! Initialize random number generator
    call random_seed()

    ! Generate random points and count the number of hits
    n_hits = 0
    do i = 1, n_points
      call random_number(x)
      call random_number(y)
      if (is_inside_unit_circle_dp(x, y)) then
        n_hits = n_hits + 1
      end if
    end do

  end subroutine generate_points_dp

  function is_inside_unit_circle_dp(x, y) result(inside)
    double precision, intent(in) :: x, y
    logical :: inside

    ! Determine whether the point (x,y) is inside the unit circle
    inside = (x**2 + y**2 < 1.0)

  end function is_inside_unit_circle_dp


end module monte_carlo_module_dp

module counts_hits_module
  implicit none

contains

  function count_hits(n_hits, n_points) result(pi)
    integer, intent(in) :: n_hits, n_points
    double precision :: pi

    ! Calculate the value of pi using the number of hits and the total number of points
    pi = real(n_hits) / real(n_points)

  end function count_hits

end module counts_hits_module

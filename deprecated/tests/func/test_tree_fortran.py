"""
Unit test for tree.py file for fortran code
"""

import pytest
import os

import networkx as nx
from pathlib import Path
from maraudersmap.tree import (
    get_tree_nx_file,
    _rec_get_tree_nx_folder,
    _rec_node_stats,
    get_tree,
)


def test_get_tree_nx_file(datadir):
    """Testing if we have the right nodes and edges, sorted list to avoid errors in nodes positionning"""
    file = os.path.join(datadir, "funsub.f90")
    node_key = "funsub.f90"
    graph = nx.DiGraph()
    graph = get_tree_nx_file(file, node_key, graph)
    edges = sorted(list(graph.edges))
    expected_edges = [
        ("funsub.f90", "funsub.f90:Area"),
        ("funsub.f90", "funsub.f90:Height"),
        ("funsub.f90", "funsub.f90:Main"),
        ("funsub.f90", "funsub.f90:Perimeter"),
        ("funsub.f90", "funsub.f90:Theta"),
    ]

    assert sorted(list(graph.nodes)) == [
        "funsub.f90",
        "funsub.f90:Area",
        "funsub.f90:Height",
        "funsub.f90:Main",
        "funsub.f90:Perimeter",
        "funsub.f90:Theta",
    ]
    assert edges == expected_edges


def test_get_tree_nx_file_fail_utf8(datadir):
    """Test the try expect for non utf8 file"""
    file = os.path.join(datadir, "binary")
    node_key = "binary"
    graph = nx.DiGraph()
    graph = get_tree_nx_file(file, node_key, graph)

    assert list(graph.nodes) == ["binary"]


def test_get_tree_nx_file_hidden_file(datadir):
    """Test the try expect for hidden file"""
    file = os.path.join(datadir, ".hidden")
    node_key = ".hidden"
    graph = nx.DiGraph()
    graph = get_tree_nx_file(file, node_key, graph)

    assert list(graph.nodes) == []


def test_rec_get_tree_nx_folder(datadir):
    """Test of get_tree_nx starting from a folder"""
    repo_path = Path(datadir)
    code_name = "func"
    graph = nx.DiGraph()
    graph = _rec_get_tree_nx_folder(repo_path, repo_path, code_name, graph)
    assert sorted(list(graph.nodes)) == [
        "test_tree_fortran/binary",
        "test_tree_fortran/dummy.yml",
        "test_tree_fortran/estimate_pi.f90",
        "test_tree_fortran/estimate_pi.f90:__script__",
        "test_tree_fortran/funsub.f90",
        "test_tree_fortran/funsub.f90:Area",
        "test_tree_fortran/funsub.f90:Height",
        "test_tree_fortran/funsub.f90:Main",
        "test_tree_fortran/funsub.f90:Perimeter",
        "test_tree_fortran/funsub.f90:Theta",
        "test_tree_fortran/modules",
        "test_tree_fortran/modules/empty_folder",
        "test_tree_fortran/modules/monte_carlo_module.f90",
        "test_tree_fortran/modules/monte_carlo_module.f90:monte_carlo_module::count_hits",
        "test_tree_fortran/modules/monte_carlo_module.f90:monte_carlo_module::generate_points",
        "test_tree_fortran/modules/monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
    ]


def test_rec_node_stats(datadir):
    """
    Test modification of nodes parameters through graph
    """
    file = os.path.join(datadir, "funsub.f90")
    file_key = "funsub.f90"
    graph = nx.DiGraph()
    graph = get_tree_nx_file(file, file_key, graph)
    _rec_node_stats(graph, file_key)

    assert sorted(graph.nodes["funsub.f90"]["module"]) == [
        "Area",
        "Height",
        "Main",
        "Perimeter",
        "Theta",
    ]
    assert graph.nodes["funsub.f90"]["size"] == 36
    assert graph.nodes["funsub.f90"]["ccn"] == 1.0
    assert graph.nodes["funsub.f90"]["leaf"] == False
    assert graph.nodes["funsub.f90"]["soften"] == True
    assert graph.nodes["funsub.f90:Area"]["leaf"] == True
    assert graph.nodes["funsub.f90:Height"]["leaf"] == True
    assert graph.nodes["funsub.f90:Main"]["leaf"] == True
    assert graph.nodes["funsub.f90:Perimeter"]["leaf"] == True
    assert graph.nodes["funsub.f90:Theta"]["leaf"] == True


def test_rec_node_stats_with_empty_folder(datadir):
    repo_path = Path(os.path.join(datadir, "modules"))
    code_name = "test_tree_fortran"
    node_key = "modules"
    graph = nx.DiGraph()
    main_folder = repo_path.relative_to(repo_path.parents[0]).as_posix()
    graph.add_node(main_folder, name=main_folder, path=repo_path.as_posix())
    graph = _rec_get_tree_nx_folder(repo_path, repo_path, code_name, graph)
    _rec_node_stats(graph, node_key)

    assert sorted(list(graph.nodes)) == [
        "modules",
        "modules/empty_folder",
        "modules/monte_carlo_module.f90",
        "modules/monte_carlo_module.f90:monte_carlo_module::count_hits",
        "modules/monte_carlo_module.f90:monte_carlo_module::generate_points",
        "modules/monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
    ]
    assert graph.nodes["modules/empty_folder"]["size"] == 1
    assert graph.nodes["modules/empty_folder"]["ccn"] == 1
    assert graph.nodes["modules/empty_folder"]["empty_folder"] == True


def test_get_tree_file(datadir):
    """Test of the whole routine for a single file, making sure we ahve the right number of nodes"""
    path = os.path.join(datadir, "modules/monte_carlo_module.f90")
    code_name = "modules"
    graph = get_tree(path, code_name)
    expected_nodes = [
        "monte_carlo_module.f90",
        "monte_carlo_module.f90:monte_carlo_module::count_hits",
        "monte_carlo_module.f90:monte_carlo_module::generate_points",
        "monte_carlo_module.f90:monte_carlo_module::is_inside_unit_circle",
    ]
    print(sorted(list(graph.nodes())))
    assert sorted(list(graph.nodes())) == expected_nodes


def test_get_tree_folder(datadir):
    """Test of the whole routine, making sure with have the right number of nodes"""
    code_name = "test_tree_fortran"
    graph = get_tree(datadir, code_name)
    assert len(graph.nodes()) == 17

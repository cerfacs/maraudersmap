program estimate_pi
  use monte_carlo_module
  implicit none

  integer :: n_points, n_hits
  real :: pi_estimate

  ! Set the number of points to use in the Monte Carlo simulation
  n_points = 1000000

  ! Generate the points and count the number of hits
  call generate_points(n_points, n_hits)

  ! Estimate the value of pi
  pi_estimate = 4.0 * count_hits(n_hits, n_points)

  ! Print the estimate
  print *, 'Estimate of pi:', pi_estimate

end program estimate_pi


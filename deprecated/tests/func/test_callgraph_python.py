"""
Func test for callgraph.py file for python code
"""

import pytest
import os

import networkx as nx
from pathlib import Path
from maraudersmap.tree import get_tree
from maraudersmap.imports import get_importsgraph
from maraudersmap.callgraph import get_calls, analyze_files, get_callgraph


def test_get_calls1():
    """Testing the parser for routine content with python object and method"""
    content = [
        "def main():\n",
        '    """Instantiate an object"""\n',
        "    aaa = myObject()\n",
        "    print(aaa.meth1(42))\n",
        "\n",
    ]
    func_calls = get_calls(content)
    expected_func = ["myObject", "aaa.meth1", "print"]
    assert sorted(func_calls) == sorted(expected_func)


def test_get_calls2():
    """Testing the parser for routine content with python nested func"""
    content = [
        "def fun_bar(aaa, bbb):\n",
        '    """Calls a function within a function call"""\n',
        "    fun_foo(fun_void(), 42)\n",
        "    pass\n",
        "\n",
    ]
    func_calls = get_calls(content)
    expected_func = ["fun_foo", "fun_void"]
    assert sorted(func_calls) == sorted(expected_func)


def test_analyze_files(datadir):
    """Testing creation of called func database"""
    code_name = "test_callgraph_python"
    file = os.path.join(datadir, "funs.py")
    tree_graph = get_tree(file, code_name)

    node = tree_graph.nodes["funs.py:fun_bar"]
    func_calls = {}
    func_calls = analyze_files(node, func_calls)
    assert sorted(func_calls["funs.py:fun_bar"]) == sorted(["fun_void", "fun_foo"])


def test_get_callgraph1(datadir):
    """Testing the general creation of callgraph for a repo"""
    code_name = "test_callgraph_python"
    imports_graph = get_importsgraph(datadir, code_name)
    tree_graph = get_tree(datadir, code_name)

    callgraph = get_callgraph(tree_graph, imports_graph)
    expected_nodes = [
        "test_callgraph_python/dummy.yml",
        "test_callgraph_python/funs.py:fun_bar",
        "test_callgraph_python/funs.py:fun_ext",
        "test_callgraph_python/funs.py:fun_foo",
        "test_callgraph_python/funs.py:fun_void",
        "test_callgraph_python/funs.py:fun_with_obj",
        "test_callgraph_python/funs.py:main",
        "test_callgraph_python/funs.py:void",
        "test_callgraph_python/objs.py:__init__",
        "test_callgraph_python/objs.py:external_fun",
        "test_callgraph_python/objs.py:meth1",
        "test_callgraph_python/objs.py:meth2",
        "test_callgraph_python/sample.py:addition",
        "test_callgraph_python/sample.py:fuuun",
        "test_callgraph_python/sample.py:maybemaybenot",
        "test_callgraph_python/sample.py:toobig",
        "test_callgraph_python/script/funs_script.py:__script__",
    ]
    expected_edges = [
        (
            "test_callgraph_python/funs.py:fun_bar",
            "test_callgraph_python/funs.py:fun_foo",
        ),
        (
            "test_callgraph_python/funs.py:fun_bar",
            "test_callgraph_python/funs.py:fun_void",
        ),
        (
            "test_callgraph_python/funs.py:fun_ext",
            "test_callgraph_python/objs.py:external_fun",
        ),
        (
            "test_callgraph_python/funs.py:fun_foo",
            "test_callgraph_python/funs.py:fun_void",
        ),
        ("test_callgraph_python/funs.py:fun_foo", "test_callgraph_python/funs.py:void"),
        (
            "test_callgraph_python/funs.py:fun_with_obj",
            "test_callgraph_python/objs.py:meth1",
        ),
        ("test_callgraph_python/funs.py:main", "test_callgraph_python/objs.py:meth1"),
        (
            "test_callgraph_python/objs.py:meth1",
            "test_callgraph_python/objs.py:external_fun",
        ),
        (
            "test_callgraph_python/sample.py:fuuun",
            "test_callgraph_python/sample.py:addition",
        ),
        (
            "test_callgraph_python/sample.py:fuuun",
            "test_callgraph_python/sample.py:maybemaybenot",
        ),
        (
            "test_callgraph_python/sample.py:fuuun",
            "test_callgraph_python/sample.py:toobig",
        ),
    ]
    assert sorted(list(callgraph.nodes())) == expected_nodes
    assert sorted(list(callgraph.edges())) == expected_edges


def test_get_callgraph2(datadir):
    """Testing the general creation of callgraph for a single file"""
    code_name = "test_callgraph_python"
    repo_path = os.path.join(datadir, "funs.py")
    imports_graph = get_importsgraph(repo_path, code_name)
    tree_graph = get_tree(repo_path, code_name)

    callgraph = get_callgraph(tree_graph, imports_graph)
    expected_nodes = [
        "funs.py:fun_bar",
        "funs.py:fun_ext",
        "funs.py:fun_foo",
        "funs.py:fun_void",
        "funs.py:fun_with_obj",
        "funs.py:main",
        "funs.py:void",
    ]
    expected_edges = [
        ("funs.py:fun_bar", "funs.py:fun_foo"),
        ("funs.py:fun_bar", "funs.py:fun_void"),
        ("funs.py:fun_foo", "funs.py:fun_void"),
        ("funs.py:fun_foo", "funs.py:void"),
    ]
    assert sorted(list(callgraph.nodes())) == expected_nodes
    assert sorted(list(callgraph.edges())) == expected_edges


def test_get_callgraph3_killswitch_off(datadir):
    """Testing the creation of callgraph without the killswitch on"""
    code_name = "test_callgraph_python"
    repo_path = os.path.join(datadir, "funs.py")
    imports_graph = get_importsgraph(repo_path, code_name)
    tree_graph = get_tree(repo_path, code_name)

    callgraph = get_callgraph(tree_graph, imports_graph, killswitch=False)
    expected_nodes = [
        "external_fun",
        "funs.py:fun_bar",
        "funs.py:fun_ext",
        "funs.py:fun_foo",
        "funs.py:fun_void",
        "funs.py:fun_with_obj",
        "funs.py:main",
        "funs.py:void",
        "meth1",
        "myObject",
        "print",
    ]
    expected_edges = [
        ("funs.py:fun_bar", "funs.py:fun_foo"),
        ("funs.py:fun_bar", "funs.py:fun_void"),
        ("funs.py:fun_ext", "external_fun"),
        ("funs.py:fun_foo", "funs.py:fun_void"),
        ("funs.py:fun_foo", "funs.py:void"),
        ("funs.py:fun_with_obj", "meth1"),
        ("funs.py:main", "meth1"),
        ("funs.py:main", "myObject"),
        ("funs.py:main", "print"),
    ]
    assert sorted(list(callgraph.nodes())) == expected_nodes
    assert sorted(list(callgraph.edges())) == expected_edges

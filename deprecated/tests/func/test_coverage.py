"""
Unit test for tree.py file for python code
"""


from maraudersmap.coverage import unroll_coverage


IN_COV = {
    "files": [ # list of files
        {      # each item concern one file
            "file": "AMR_INTERFACE/mod_amr_metric.f90",
            "functions": [],
            "lines": [ # list of lines
                {
                    "branches": [],           # is part of a branch
                    "count": 1,               # was it covered or not
                    "gcovr/excluded": False,  # is it excluded from coverage
                    "gcovr/noncode": True,    # is it code 
                    "line_number": 32         # the line nb
                },
                {
                    "branches": [],
                    "count": 0,
                    "gcovr/excluded": True,
                    "gcovr/noncode": False,
                    "line_number": 41
                },
                {
                    "branches": [],
                    "count": 0,
                    "gcovr/excluded": False,
                    "gcovr/noncode": False,
                    "line_number": 41
                },
                {
                    "branches": [
                        {                # branch A    
                            "count": 0,              # was it taken
                            "fallthrough": False,    # ??????
                            "throw": False           # ??????
                        },
                        {               # branch B  
                            "count": 1,
                            "fallthrough": False,
                            "throw": False
                        }
                    ],
                    "count": 1,
                    "gcovr/excluded": False,
                    "gcovr/noncode": False,
                    "line_number": 63
                }
            ]
        }
    ]
}

OUT_COV = {
    'AMR_INTERFACE/mod_amr_metric.f90': {
        'lines': (
            1,        # lines covered
            2,        # lines in file
            0.5       # ratio 
            ), 
    }
}

def test_coverage():
    """Testing if we have the right nodes and edges, sorted to avoid names wrong positionning"""
    
    assert unroll_coverage(IN_COV) == OUT_COV


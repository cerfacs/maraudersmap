"""
Func test for macro_tree.py file for fortran code
"""

import os
from maraudersmap.tree import get_tree
from maraudersmap.macro_tree import (
    _rec_to_macro_tree,
    _rec_node_to_erase,
    get_macro_tree,
)


def test_rec_node_to_erase(datadir):
    """Test to find the list of nodes to be erased"""
    path = os.path.join(datadir, "funsub.f90")
    code_name = "test_macro_tree_fortran"
    graph = get_tree(path, code_name)
    expected_erase = [
        "funsub.f90:Main",
        "funsub.f90:Area",
        "funsub.f90:Theta",
        "funsub.f90:Height",
        "funsub.f90:Perimeter",
    ]
    assert _rec_node_to_erase(graph, "funsub.f90") == expected_erase


def test_rec_to_macro_tree_file(datadir):
    """Test to switch leaves to parent node"""
    path = os.path.join(datadir, "funsub.f90")
    code_name = "test_tree_fortran"
    graph = get_tree(path, code_name)

    for node in _rec_node_to_erase(graph, "funsub.f90"):
        graph.remove_node(node)
   
    # Erasing node to be erased for convenience
    _rec_to_macro_tree(graph, "funsub.f90")
    assert len(graph.nodes()) == 1
    assert graph.nodes["funsub.f90"]["leaf"] == True


def test_rec_to_macro_tree_folder(datadir):
    """Test to switch leaves to parent node"""
    code_name = "test_macro_tree_fortran"
    graph = get_tree(datadir, code_name)

    for node in _rec_node_to_erase(graph, "test_macro_tree_fortran"):
        graph.remove_node(node)
    # Erasing node to be erased for convenience
    _rec_to_macro_tree(graph, "test_macro_tree_fortran")
    for node in graph.nodes:
        print(graph.nodes[node])
    assert graph.nodes["test_macro_tree_fortran"]["leaf"] == False
    assert graph.nodes["test_macro_tree_fortran/funsub.f90"]["leaf"] == True
    assert graph.nodes["test_macro_tree_fortran/estimate_pi.f90"]["leaf"] == True
    assert graph.nodes["test_macro_tree_fortran/modules"]["leaf"] == False
    assert (
        graph.nodes["test_macro_tree_fortran/modules/monte_carlo_module.f90"]["leaf"]
        == True
    )


def test_get_macro_tree(datadir):
    """Test whole routine, making sure we have the right number of nodes"""
    code_name = "test_macro_tree_fortran"
    graph = get_macro_tree(datadir, code_name)
    assert len(graph) == 5

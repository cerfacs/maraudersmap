PROGRAM Triangle
    IMPLICIT NONE
    CALL Main()

END PROGRAM Triangle

SUBROUTINE Main()
    IMPLICIT NONE
    REAL :: a, b, c, Area, Per
    PRINT *, 'Welcome, please enter the&
                &lengths of the 3 sides.'
    READ *, a, b, c
    PRINT *, 'Triangle''s area:  ', Area(a,b,c)
    per = 0.
    CALL Perimeter(a,b,c,per)
    PRINT *, 'Triangle''s perimeter:  ', per
END SUBROUTINE Main

FUNCTION Area(x,y,z)
    IMPLICIT NONE
    REAL :: Area            ! function type
    REAL, INTENT( IN ) :: x, y, z
    REAL :: Height, my_h, Theta
    my_h = Height(x,Theta(x,y,z))
    Area = 0.5*y*my_h
END FUNCTION Area

FUNCTION Theta(x,y,z)
    IMPLICIT NONE
    REAL :: Theta            ! function type
    REAL, INTENT( IN ) :: x, y, z
    Theta = ACOS((x**2+y**2-z**2)/(2.0*x*y))
END FUNCTION Theta

FUNCTION Height(x,theta)
    IMPLICIT NONE
    REAL :: Height            ! function type
    REAL, INTENT( IN ) :: x, theta
    Height = x*SIN(theta)
END FUNCTION Height

SUBROUTINE Perimeter(x,y,z,per)
    REAL, INTENT( IN ) :: x, y, z
    REAL, INTENT( OUT ) :: per
    per = x + y + z
END SUBROUTINE Perimeter
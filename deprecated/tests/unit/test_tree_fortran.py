"""
Unit test for tree.py file for fortran code
"""

import pytest
import os

import networkx as nx
from maraudersmap.tree import (
    lizard_analysis,
    parse_function,
    add_void_node,
    add_fullnode,
)


def test_lizard_analysis(datadir):
    """Testing creation of lizard database"""
    file = os.path.join(datadir, "funsub.f90")

    with open(file, "r", encoding="utf-8") as fin:
        code = fin.read()

    lizard_out = lizard_analysis(file, code)

    assert len(lizard_out.__dict__["function_list"]) == 5


def test_parse_function(datadir):
    """Test the accuracy of values obtained"""
    file = os.path.join(datadir, "funsub.f90")

    with open(file, "r", encoding="utf-8") as fin:
        code = fin.read()

    lizard_out = lizard_analysis(file, code)
    func_names, sizes, ccn, line_start, line_end, callables = parse_function(lizard_out,code)

    assert func_names == ["Main", "Area", "Theta", "Height", "Perimeter"]
    assert sizes == [11, 8, 6, 6, 5]
    assert ccn == [1, 1, 1, 1, 1]
    assert line_start == [6, 18, 27, 34, 41]
    assert line_end == [17, 26, 33, 40, 46]
    assert callables ==  [['area', 'perimeter'], ['height', 'theta'], ['acos'], ['sin'], []]


@pytest.mark.parametrize("analyzed", (True, False))
def test_add_void_node(analyzed):
    graph = nx.DiGraph()
    graph = add_void_node(graph, "dummy", "dummy_path", analyzed=analyzed)

    assert graph.nodes["dummy"]["name"] == "dummy"
    assert graph.nodes["dummy"]["path"] == "dummy_path"
    assert graph.nodes["dummy"]["size"] == 1
    assert graph.nodes["dummy"]["ccn"] == 1
    assert graph.nodes["dummy"]["line_start"] == None
    assert graph.nodes["dummy"]["line_end"] == None
    assert graph.nodes["dummy"]["analyzed"] == analyzed
    assert graph.nodes["dummy"]["func"] == []


def test_add_fullnode():
    graph = nx.DiGraph()
    graph = add_fullnode(graph, "dummy", "dummy_func", "dummy_path", 10, 2, 1, 25, ["cos", "sin"])

    assert graph.nodes["dummy:dummy_func"]["name"] == "dummy:dummy_func"
    assert graph.nodes["dummy:dummy_func"]["path"] == "dummy_path"
    assert graph.nodes["dummy:dummy_func"]["size"] == 10
    assert graph.nodes["dummy:dummy_func"]["ccn"] == 2
    assert graph.nodes["dummy:dummy_func"]["line_start"] == 1
    assert graph.nodes["dummy:dummy_func"]["line_end"] == 25
    assert graph.nodes["dummy:dummy_func"]["func"] == "dummy_func"
    assert graph.nodes["dummy:dummy_func"]["callables"] == ["cos", "sin"]
    assert list(graph.edges()) == [("dummy", "dummy:dummy_func")]

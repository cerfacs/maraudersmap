""" Unit test for imports.py for python code"""

import pytest
import os

import networkx as nx
from maraudersmap.imports import python_imports


def test_python_imports_1(datadir):
    """Test for a simple python script"""
    code_name = "funs_script.py"
    filename = os.path.join(datadir, "funs_script.py")
    with open(filename, "r") as fin:
        code = fin.readlines()
    fimports = {}
    fimports = python_imports(code, fimports, filename, code_name)

    assert len(fimports["funs_script.py"]) == 3
    assert list(fimports["funs_script.py"].keys()) == [
        "numpy",
        "test_imports_python.objs",
        "test_import_python.funs",
    ]
    assert fimports["funs_script.py"]["numpy"] == ["np"]
    assert fimports["funs_script.py"]["test_imports_python.objs"] == ["myObject"]
    assert fimports["funs_script.py"]["test_import_python.funs"] == [
        "fun_foo",
        "fun_ext",
        "fun_void",
        "fun_bar",
        "main",
        "fun_with_obj",
    ]


def test_python_imports_2(datadir):
    """Test for a common python file with functions"""
    code_name = "funs.py"
    filename = os.path.join(datadir, "funs.py")
    with open(filename, "r") as fin:
        code = fin.readlines()
    fimports = {}
    fimports = python_imports(code, fimports, filename, code_name)

    assert len(fimports["funs.py"]) == 2
    assert list(fimports["funs.py"].keys()) == [
        "numpy",
        "test_imports_python.objs",
    ]
    assert fimports["funs.py"]["numpy"] == ["np"]
    assert fimports["funs.py"]["test_imports_python.objs"] == [
        "myObject",
        "external_fun",
    ]

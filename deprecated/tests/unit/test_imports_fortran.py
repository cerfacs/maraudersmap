""" Unit test for imports.py for fortran code"""

import pytest
import os

import networkx as nx
from maraudersmap.imports import fortran_imports, _rec_spec_fimports


def test_fortran_imports_1(datadir):
    """Test for a simple fortran file"""
    code_name = "estimate_pi.f90"
    filename = os.path.join(datadir, "estimate_pi.f90")
    with open(filename, "r") as fin:
        code = fin.readlines()
    fimports = {}
    fimports = fortran_imports(code, fimports, filename, code_name)

    assert len(fimports["estimate_pi.f90"]) == 3
    assert sorted(list(fimports["estimate_pi.f90"].keys())) == [
        "counts_hits_module",
        "monte_carlo_module_dp",
        "monte_carlo_module_h",
    ]
    assert fimports["estimate_pi.f90"]["counts_hits_module"] == ["count_hits"]
    assert fimports["estimate_pi.f90"]["monte_carlo_module_h"] == []
    assert fimports["estimate_pi.f90"]["monte_carlo_module_dp"] == [
        "generate_points_dp",
        "is_inside_unit_circle_dp",
    ]


def test_fortran_imports_2(datadir):
    """Test for a fortran header"""
    code_name = "monte_carlo_module_h.f90"
    filename = os.path.join(datadir, "monte_carlo_module_h.f90")
    with open(filename, "r") as fin:
        code = fin.readlines()
    fimports = {}
    fimports = fortran_imports(code, fimports, filename, code_name)

    assert len(fimports["monte_carlo_module_h.f90"]) == 1
    assert list(fimports["monte_carlo_module_h.f90"].keys()) == ["monte_carlo_module"]
    assert fimports["monte_carlo_module_h.f90"]["monte_carlo_module"] == [
        "generate_points",
        "is_inside_unit_circle",
    ]


def test_rec_spec_imports():
    """Test multiline import"""
    code = ["func1, &", "func2, func3, &", "func4"]
    imports_ = []
    specific_use = code[0].strip().split(",")
    specific_use = _rec_spec_fimports(code, specific_use, 0)

    assert specific_use == ["func1", "func2", "func3", "func4"]

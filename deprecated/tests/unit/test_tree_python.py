"""
Unit test for tree.py file for python code
"""

import pytest
import os

import networkx as nx
from maraudersmap.tree import lizard_analysis, parse_function


def test_lizard_analysis(datadir):
    """Testing creation of lizard database"""
    file = os.path.join(datadir, "funs.py")

    with open(file, "r", encoding="utf-8") as fin:
        code = fin.read()

    lizard_out = lizard_analysis(file, code)

    assert len(lizard_out.__dict__["function_list"]) == 6


def test_parse_function(datadir):
    """Test the accuracy of values obtained"""
    file = os.path.join(datadir, "funs.py")

    with open(file, "r", encoding="utf-8") as fin:
        code = fin.read()

    lizard_out = lizard_analysis(file, code)
    func_names, sizes, ccn, line_start, line_end, callables = parse_function(lizard_out, code)

    assert func_names == [
        "fun_foo",
        "fun_ext",
        "fun_void",
        "fun_bar",
        "main",
        "fun_with_obj",
    ]
    assert sizes == [8, 3, 2, 3, 3, 2]
    assert ccn == [1, 1, 1, 1, 1, 1]
    assert line_start == [4, 14, 19, 23, 28, 33]
    assert line_end == [13, 18, 22, 27, 32, 36]


def test_parse_function_script(datadir):
    """Test the identification of script file"""
    file = os.path.join(datadir, "funs_script.py")

    with open(file, "r", encoding="utf-8") as fin:
        code = fin.read()

    lizard_out = lizard_analysis(file, code)
    func_names, sizes, ccn, line_start, line_end, callables = parse_function(lizard_out, code)

    assert func_names == ["__script__"]
    assert sizes == [8]
    assert ccn == [1]
    assert line_start == [0]
    assert line_end == [9]
    assert callables == [[]]
    

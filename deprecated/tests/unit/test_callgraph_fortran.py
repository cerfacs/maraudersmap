"""
Unit test for callgraph.py file for fortran code
"""

import pytest
from maraudersmap.callgraph import _rec_call


def test_rec_calls():
    """Testing the parser for routine content with fortran script"""
    line = "  call generate_points(n_points, n_hits)\n"
    called_list = []
    called_list = _rec_call(called_list, line)
    expected_func = ["generate_points"]

    assert sorted(called_list) == sorted(expected_func)


def test_get_calls2():
    """Testing the parser for routine content with fortran nested call"""
    line = "      if (is_inside_unit_circle(x, y)) then\n"
    called_list = []
    called_list = _rec_call(called_list, line)
    expected_func = ["if", "is_inside_unit_circle"]

    assert sorted(called_list) == sorted(expected_func)

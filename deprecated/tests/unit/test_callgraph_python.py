"""
Unit test for callgraph.py file for python code
"""

import pytest
from maraudersmap.callgraph import _rec_call


def test_rec_call1():
    """Testing the parser for routine content with python method"""
    line = "    print(aaa.meth1(42))\n"
    called_list = []
    called_list = _rec_call(called_list, line)
    expected_func = ["aaa.meth1", "print"]

    assert sorted(called_list) == sorted(expected_func)


def test_rec_call2():
    """Testing the parser for routine content with python nested func"""
    line = "    fun_foo(fun_void(), 42)\n"
    called_list = []
    called_list = _rec_call(called_list, line)
    expected_func = ["fun_foo", "fun_void"]

    assert sorted(called_list) == sorted(expected_func)


def test_rec_call3():
    """Testsing with False positive"""
    line = "    (fun_void(), 42)\n"
    called_list = []
    called_list = _rec_call(called_list, line)
    expected_func = ["fun_void"]

    assert sorted(called_list) == sorted(expected_func)

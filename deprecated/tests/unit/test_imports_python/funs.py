"""This code is a sample for callgraphs"""
import numpy as np

# Imports are reduce to the current folder for tests purposes it thus emulates the folder as a repo
# This is mandatory in order to run the tests properly
from test_imports_python.objs import myObject, external_fun


def fun_foo(aaa: str, bbb: int, ccc: bool = True):
    """Basic function calling a function"""
    fun_void()
    pass
    pass
    pass
    pass
    pass
    pass


def fun_ext():
    """Basic function calling a function of another module"""
    external_fun(43)
    pass


def fun_void():
    """Simplest function"""
    return "dummy"


def fun_bar(aaa, bbb):
    """Calls a function within a function call"""
    fun_foo(fun_void(), 42)
    pass


def main():
    """Instantiate an object"""
    aaa = myObject()
    print(aaa.meth1(42))


def fun_with_obj(aaa: myObject):
    """Use an object in args"""
    aaa.meth1(42)

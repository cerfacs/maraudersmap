
# New test global
test:
	pytest --cov=maraudersmap -v tests 

testhtml:
	pytest --cov=maraudersmap -v tests --cov-config=.coveragerc  --cov-report html

# New test unit
testu:
	py.test --cov=maraudersmap -v tests/unit 

test2uhtml:
	py.test --cov=maraudersmap -v tests/unit --cov-config=.coveragerc  --cov-report html

# New test funct
testf:
	py.test --cov=maraudersmap -v tests/func 

testfhtml:
	py.test --cov=maraudersmap -v tests/func --cov-config=.coveragerc  --cov-report html

# Vanilla
lint:
	pylint maraudersmap

doc:
	cd docs && make html

wheel:
	rm -rf build
	rm -rf dist
	python setup.py sdist bdist_wheel

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload dist/* -u __token__


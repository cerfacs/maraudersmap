# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).

## [0.4.1] 2025 / 02 / 25

### Changed

- removal of self_connect edges in call_graphs

### Fixed

-  fix errors in the get subgraph
- fix error when stating a null context

## [0.4.0] 2024 / 11 / 29

### Added

- Support direct Valgrind 3.18 traces for graph plotting
- New graph coloration `grep` to color graphs by one or two sets of greps
- Support Valgrind 3.18 traces for callgraph generation
- Can support CPP definitions to generate graphs on different codebases based on CPP

### Changed

- The pyvis size is now much smaller to ensure faster Barnes-Hut convergence

### Fixed

- Adapt to mmap 0.4.2 and beyond
- remove deprecated args in CLI


## [0.3.0] 2024 / 07 / 29

### Added

-  First support of Tucan's 0.3.2 imports new feature. Use `im-gen`and `im-show` to explore this. Warning : less coloring than cg-show are available.  `im-fast` not added yet.

### Fixed

- correct Tucan version compatibilty

## [0.2.1] 2024 / 07 / 02

### Fixed

- fix unwanted dependency
- allow pdf format in Plotly backend
- apply common prefix deletion in label to all graphs now

## [0.2.0] 2024 / 06 / 10


### Added

- fast generation of callgraphs using `cg-fast` (no input file required)
- support for plotly-dash rendering
- support for 3D forces on layouts
- plenty of tests on the layouts

### Changed

- cli is now using a simplified version `cg-gen`,`cg-show` and `cg-fast`
- color handling is now unified and done before the graphs

### Fixed

- various corrections on the spring model 

### Deprecated

[-]


## [0.1.0] 2024 / 05 / 14


### Added

- single comment `mmap callgraph` to regenerate callgraphs

### Changed

- parser support now using [tucan](https://gitlab.com/cerfacs/tucan)

### Fixed

[ - ]

### Deprecated

- treegraph, importgraph are not used anymore

## [0.0.0] 2022 / 09 /01

### Added

- tree graph (goes to functions level within files)
- tree graph (mmap tree --out pyvis) dumps a .html file with nodes
- tree graph (mmap tree) is a circular package view of repo structure
- tree graph (mmap tree --macro) stop at file lvl
- implemented fortran callgraph
- implemented nobvisual visualization for tree graph


### Changed

[ - ]

### Fixed

[ - ]

### Deprecated

[ - ]


